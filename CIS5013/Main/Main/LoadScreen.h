#pragma once
#include "Scene.h"
#include "ObjExaminer.h"
#include "Game.h"
#include "MenuController.h"
#include "ImageRect.h"

class LoadScreen :
	public Scene
{
public:
	LoadScreen();
	~LoadScreen();

	void run();
	void load(Game* gameRefr);
	void loadTextures() {}
protected:
	ImageRect * loadImg;
	MenuController * menuController;
};

