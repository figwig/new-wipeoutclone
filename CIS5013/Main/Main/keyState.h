#pragma once

#include <cstdint>

typedef  uint32_t Keystate;

// These values declare which bits in KeyFlags are set / unset when a key is pressed / released respecively
enum Keys {

	Up    =		 0b0000001,
	Down  =		 0b0000010,
	Left  =		 0b0000100,
	Right =		 0b0001000,
	R     =		 0b0010000,
	F     =		 0b0100000,
	Space =	     0b10000000,
	num1	  =  0b100000000,
	num2 =       0b1000000000
};
