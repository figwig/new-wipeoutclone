#include "Object3D.h"
#include "Game.h"
#include "texture_loader.h"
#include "VertexData.h"
#include "Point2f.h"
#include "tiny_obj_loader.h"
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <stdio.h>
#include "VAOData.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include "Light.h"

using namespace CoreStructures;


void Object3D::update(double newDeltaTime)
{
	//set deltaTime
	deltaTime = newDeltaTime;
}

void Object3D::lateUpdate(double deltaTime)
{
	//The matricies of rotation to apply to the object

	if (parent != nullptr)
	{

		
		//Set up matricies representing the parent
		pR = glm::toMat4(
			glm::quat(glm::vec3(0                 , parent->getRot().y, 0))*
			glm::quat(glm::vec3(parent->getRot().x,0                  , 0))*
			glm::quat(glm::vec3(0                 , 0                 , parent->getRot().z))
		);
			
		pT = glm::translate(glm::mat4(1),glm::vec3(parent->getPos().x, parent->getPos().y, parent->getPos().z));
		
		//Set up matricies representing overall transform and rotation
		R = pR;
		T = pT;

		S = GUMatrix4::scaleMatrix(scaleX, scaleY, scaleZ);
	}
	else
	{
		T =getTransMat();
		S = GUMatrix4::scaleMatrix(scaleX, scaleY, scaleZ);

		R = glm::toMat4(
			glm::quat(   glm::vec3(0.0f, thetaY, 0.0f)   )*
			glm::quat(   glm::vec3(thetaX, 0.0f, 0.0f)   )*
			glm::quat(   glm::vec3(0.0f, 0.0f, thetaZ))  );
	}
}



void Object3D::draw(glm::mat4 camera)
{
	glUseProgram(defaultShaders);

	//Set the texture units
	GLuint locTex0 = glGetUniformLocation(thisConfig.mainShaders, "textureImage");
	GLuint locTex1 = glGetUniformLocation(thisConfig.mainShaders, "specularImage");
	GLuint locTex2 = glGetUniformLocation(thisConfig.mainShaders, "normalMap");

	glUniform1i(locTex0, 0);
	glUniform1i(locTex1, 1);
	glUniform1i(locTex2, 2);
	
	glUniformMatrix4fv(thisConfig.locT, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(thisConfig.locR, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(thisConfig.locS, 1, GL_FALSE, (GLfloat*)&S);
	glUniformMatrix4fv(thisConfig.locC, 1, GL_FALSE, (GLfloat*)&camera);

	//Sort the parent heirarchy transform stuff
	GLuint posLoc = glGetUniformLocation(defaultShaders, "posRelParent");


	setLightingUniforms(camera);

	//Parent Relative Transform stuff
	if (parent == nullptr)
	{
		glUniform4f(posLoc, 0.0f, 0.0f, 0.0f, 0.0f);
	}
	else
	{
		glUniform4f(posLoc, posX, posY, posZ, 0.0f);
	}

	//Bind the model's texture to the object we're drawing
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, dataVAO->getTexture());

	//and the specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, dataVAO->getSpecMap());

	if (dataVAO->getNormalMap() != 0)
	{
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, dataVAO->getNormalMap());
	}

	glBindVertexArray(dataVAO->getVAO());
	glDrawArrays(GL_TRIANGLES, 0, dataVAO->getTotalVerts());

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	//and the specular map
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);


	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (drawTangent)
	{
		drawTangents(camera);
	}
	if (drawNormal)
	{
		drawNormals(camera);
	}
}

void Object3D::drawNormals(glm::mat4 camera)
{
	glUseProgram(thisConfig.normalShaders);


	glUniformMatrix4fv(thisConfig.nLocProj,
		1, GL_FALSE,
		(GLfloat*)&thisConfig.currLighting->persp);

	glUniformMatrix4fv(thisConfig.locTn, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(thisConfig.locRn, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(thisConfig.locSn, 1, GL_FALSE, (GLfloat*)&S);
	glUniformMatrix4fv(thisConfig.locCn, 1, GL_FALSE, (GLfloat*)&thisConfig.currLighting->cameraWithOutPersp);

	glBindVertexArray(dataVAO->getVAO());
	glDrawArrays(GL_TRIANGLES, 0, dataVAO->getTotalVerts());
}


void Object3D::drawTangents(glm::mat4 camera)
{
	glUseProgram(thisConfig.tangentShaders);


	glUniformMatrix4fv(thisConfig.tLocProj,
		1, GL_FALSE,
		(GLfloat*)&thisConfig.currLighting->persp);

	glUniformMatrix4fv(thisConfig.locTt, 1, GL_FALSE, glm::value_ptr(T));
	glUniformMatrix4fv(thisConfig.locRt, 1, GL_FALSE, (GLfloat*)&R);
	glUniformMatrix4fv(thisConfig.locSt, 1, GL_FALSE, (GLfloat*)&S);
	glUniformMatrix4fv(thisConfig.locCt, 1, GL_FALSE, (GLfloat*)&thisConfig.currLighting->cameraWithOutPersp);

	glBindVertexArray(dataVAO->getVAO());
	glDrawArrays(GL_TRIANGLES, 0, dataVAO->getTotalVerts());
}

void Object3D::setLightingUniforms(glm::mat4 camera)
{
	//Tell the shader if a normal map is being used or not
	glUniform1i(thisConfig.locUseNormalMap, (dataVAO->getNormalMap()!=0) );

	//Tell the shader how shiny this object is
	glUniform1f(thisConfig.locShiny, shinyness);

	//Tell the shader how many lights there are
	glUniform1i(thisConfig.locLightCount, thisConfig.currLighting->numberOfLights);

	//Tell the shader where the camera is
	glm::vec3 cameraPos = thisConfig.currLighting->cameraPos;
	glUniform3f(thisConfig.locCameraPos, cameraPos.x, cameraPos.y, cameraPos.z);
	
	//For each light, set the relevant factors in the shader
	for (int count = 0; count < thisConfig.currLighting->numberOfLights; count++)
	{
		//Work out the light pos based on the camera 
		glm::vec4 lightPos = thisConfig.currLighting->lights[count]->position;

		glUniform4f(thisConfig.lightingUniforms[(count*thisConfig.PARAMS_PER_LIGHT)], lightPos.x, lightPos.y, lightPos.z, lightPos.w);
		glUniform3f(thisConfig.lightingUniforms[(count * thisConfig.PARAMS_PER_LIGHT) + 1], thisConfig.currLighting->lights[count]->intensities.x, thisConfig.currLighting->lights[count]->intensities.y, thisConfig.currLighting->lights[count]->intensities.z);
		glUniform1f(thisConfig.lightingUniforms[(count * thisConfig.PARAMS_PER_LIGHT) + 2], thisConfig.currLighting->lights[count]->attenuation);
		glUniform1f(thisConfig.lightingUniforms[(count * thisConfig.PARAMS_PER_LIGHT) + 3], thisConfig.currLighting->lights[count]->ambientCoefficient);
		glUniform1f(thisConfig.lightingUniforms[(count * thisConfig.PARAMS_PER_LIGHT) + 4], thisConfig.currLighting->lights[count]->coneAngle);
		glUniform3f(thisConfig.lightingUniforms[(count * thisConfig.PARAMS_PER_LIGHT) + 5], thisConfig.currLighting->lights[count]->coneDirection.x, thisConfig.currLighting->lights[count]->coneDirection.y, thisConfig.currLighting->lights[count]->coneDirection.z);
		glUniformMatrix4fv(thisConfig.lightingUniforms[(count* thisConfig.PARAMS_PER_LIGHT) + 6], 1 ,GL_FALSE, (GLfloat*) &thisConfig.currLighting->lights[count]->finalMatrix);
	}
}

