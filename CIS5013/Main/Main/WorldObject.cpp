#include "WorldObject.h"
#include <CoreStructures/CoreStructures.h>
#define _USE_MATH_DEFINES
#include <math.h>

///<Summary>
///A world object is something that is renderer relative to 0,0,0,  usually forming a part of the 
///track or world
///</Summary>
WorldObject::WorldObject(VAOData* newData)
{
	dataVAO = newData;

	scaleX = 1.3f;
	scaleY = 1.3f;
	scaleZ = 1.3f;

	posY = -7.0f;
	posZ = -30.0f;

	thetaY = M_PI;
}

WorldObject::~WorldObject()
{


}
void WorldObject::update(double deltaTime)
{
	Object3D::update(deltaTime);
	Object3D::lateUpdate(deltaTime);
}

void WorldObject::draw(glm::mat4 camera)
{
	Object3D::draw(camera);
}