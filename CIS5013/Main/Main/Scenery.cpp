#include "Scenery.h"



Custom::Custom(VAOData* newData, glm::vec3 pos, glm::vec3 scale, glm::vec3 rot)
{

	posX = pos.x;
	posY = pos.y;
	posZ = pos.z;

	scaleX = scale.x;
	scaleY = scale.y;
	scaleZ = scale.z;

	thetaX = rot.x;
	thetaY = rot.y;
	thetaZ = rot.z;

	dataVAO = newData;

	setVolumeScale(scale);
}


Custom::~Custom()
{

}
