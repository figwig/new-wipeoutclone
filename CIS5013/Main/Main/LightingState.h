#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
class Light;

class LightingState
{
public:
	glm::vec3 cameraPos;
	int numberOfLights = 0;

	const static int MAX_LIGHTS = 12;
	Light* lights[MAX_LIGHTS] = { nullptr };

	void addLight(Light* newLight)
	{
		for (int count = 0; count < MAX_LIGHTS; count++)
		{
			if (lights[count] == nullptr )
			{
				lights[count] = newLight;
				numberOfLights++;
				return;
			}
		}
	}
	void update()
	{

	}

	glm::mat4 cameraWithOutPersp;
	glm::mat4 persp;
};