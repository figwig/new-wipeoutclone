#include "HoverJet.h"
#include <CoreStructures/CoreStructures.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <random>


HoverJet::HoverJet()
{
	generateCircle(50, radius);
	sortVAO();

	angle = angleMod * 3;

	generateSecondaryCircle(50, radius);
	sortVAO2();
}


HoverJet::~HoverJet()
{
}

void HoverJet::draw(glm::mat4 camera)
{
	if (active) {

		timePassed += deltaTime;
		glUseProgram(thisConfig.effectShaders2);

		//Tell the shaders all the relevant information this frame
		shaderUniformSetup(camera);



		if (firstCircle)
		{
			glBindVertexArray(VAO);

			glDrawArrays(GL_POINTS, 0, totalPoints);
			firstCircle = false;
		}
		else
		{
			glBindVertexArray(VAO2);

			glDrawArrays(GL_POINTS, 0, totalPoints2);
			firstCircle = true;
		}
	}
}

void HoverJet::generateCircle(int noPoints, float radius)// add z co-ord variation!
{
	std::uniform_int_distribution<int> distribution(800, 999);
	std::uniform_int_distribution<int> distribution2(180, 600);

	float interval = (M_PI * 2) / noPoints;

	points = new float[noPoints * 3];
	colors = new float[noPoints * 4];
	angles = new float[noPoints];

	int count = 0;
	int countColor = 0;
	int angleCount = 0;

	for (angle; angle < (M_PI * 2) + angleMod; angle += interval)
	{
		float blueValue = distribution(randGenerator) / 1000.0f;
		float rgValue = distribution2(randGenerator) / 1000.0f;

		points[count] = radius * cos(angle);
		colors[countColor] = rgValue;
		count++;
		countColor++;

		points[count] =0.0f ;
		colors[countColor] = rgValue;
		count++;
		countColor++;

		points[count] = radius * sin(angle);


		colors[countColor] = blueValue;
		count++;
		countColor++;

		colors[countColor] = 0.04f;
		countColor++;

		angles[angleCount] = angle;
		angleCount++;
	}

	totalPoints = noPoints;
}
void HoverJet::generateSecondaryCircle(int noPoints, float radius)// add z co-ord variation!
{
	std::uniform_int_distribution<int> distribution(800, 999);
	std::uniform_int_distribution<int> distribution2(180, 600);

	float interval = (M_PI * 2) / noPoints;

	points2 = new float[noPoints * 3];
	colors2 = new float[noPoints * 4];
	angles2 = new float[noPoints];

	int count = 0;
	int countColor = 0;
	int angleCount = 0;

	for (angle; angle < (M_PI * 2) + angleMod; angle += interval)
	{
		float blueValue = distribution(randGenerator) / 1000.0f;
		float rgValue = distribution2(randGenerator) / 1000.0f;

		points2[count] = radius * cos(angle);
		colors2[countColor] = rgValue;
		count++;
		countColor++;

		points2[count] = 0.0f;
		colors2[countColor] = rgValue;
		count++;
		countColor++;

		points2[count] = radius * sin(angle);


		colors2[countColor] = blueValue;
		count++;
		countColor++;

		colors2[countColor] = 0.04f;
		countColor++;

		angles2[angleCount] = angle;
		angleCount++;
	}

	totalPoints2 = noPoints;
}
void HoverJet::shaderUniformSetup(glm::mat4 camera)
{
	//Tell all the shaders various stuff
	//Transform Matrix
	glUniformMatrix4fv(thisConfig.locTe2, 1, GL_FALSE, (GLfloat*)&T);
	//Rotation Matrix
	glUniformMatrix4fv(thisConfig.locRe2, 1, GL_FALSE, (GLfloat*)&R);
	//Scale matrix
	glUniformMatrix4fv(thisConfig.locSe2, 1, GL_FALSE, (GLfloat*)&S);
	//Camera matrix
	glUniformMatrix4fv(thisConfig.locCe2, 1, GL_FALSE, (GLfloat*)&camera);

	//Projection matrix
	glUniformMatrix4fv(thisConfig.e2LocProj,
		1, GL_FALSE,
		(GLfloat*)&CoreStructures::GUMatrix4::infinitePerspectiveProjection
		(50.0f, 16.0f / 9.0f, 0.1f));

	//The magnitude
	glUniform1f(thisConfig.e2LocMag,
		magnitude);

	//Target Vector
	glUniform4f(thisConfig.e2LocTarget, target.x, target.y, target.z, 0.0f);

	//And the position relative to the parent
	//GLuint loc = 
	glUniform4f(thisConfig.e2LocPosRel, posX, posY, posZ, 0.0f);

	//And finally the time
	glUniform1f(thisConfig.e2LocTime, (float)timePassed);

}

void HoverJet::sortVAO()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints * 3, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &colorVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints * 4, colors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &anglesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, anglesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, angles, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	delete[] points;
	delete[] colors;
	delete[] angles;
}

void HoverJet::sortVAO2()
{
	glGenVertexArrays(1, &VAO2);
	glBindVertexArray(VAO2);

	glGenBuffers(1, &VBO2);
	glBindBuffer(GL_ARRAY_BUFFER, VBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints2 * 3, points2, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &colorVBO2);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints2 * 4, colors2, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &anglesVBO2);
	glBindBuffer(GL_ARRAY_BUFFER, anglesVBO2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints2, angles2, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	delete[] points2;
	delete[] colors2;
	delete[] angles2;
}
