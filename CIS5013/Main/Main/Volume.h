#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
class Scene;
class GameObject;
class Volume
{
public:
	Volume();
	~Volume();

	virtual void preCheckCalcs(Volume *) = 0;
	virtual bool checkPoint(glm::vec3) = 0;
	virtual bool checkVolume(Volume *) = 0;
	virtual void createVolume(glm::vec3 centre) = 0;

	int getPointsCount() { return pointsCount; }
	glm::vec3 getPointByIndex(int i) { return checkPoints[i]; }
	glm::vec3 getCentre() { return centre; }
	void checkForCollisionsScene(Scene *);
	void addBufferPoint(glm::vec3 newPoint) { checkBufferPoints[bufferIndex] = newPoint; bufferIndex +=1 ; }
	void setScale(glm::vec3 scale) { for (int count = 0; count < bufferIndex; count++) { checkPoints[count] *= scale; } }
	void setOwner(GameObject * newObject) { owner = newObject; }
protected:

	int pointsCount = 0;
	int resolution = 0;

	glm::vec3 centre= glm::vec3(0);


	glm::vec3 *checkPoints= { nullptr };

	int bufferIndex = 0;

	const static int MAX_POINTS =128000;
	glm::vec3 checkBufferPoints[MAX_POINTS] = { glm::vec3(0) };

	GameObject * owner;
};

