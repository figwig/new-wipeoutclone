#include "Clock.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <glew\glew.h>
#include <freeglut\freeglut.h>


void Clock::tick()
{
	ticks = glutGet(GLUT_ELAPSED_TIME);
	delta = ticks - lastTime;

	delay();
	lastTime = ticks;

	frames++;

	avgFPS = frames / (ticks/1000);
}

void Clock::end()
{
	std::cout << "Average FPS: " << avgFPS ;
}

void Clock::delay()
{
	if (delta < target)
	{
		Sleep(target - delta);
	}
}
