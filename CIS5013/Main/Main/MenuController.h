#pragma once
#include "GameObject.h"
#include "Game.h"

class MenuController :
	public GameObject
{
public:
	MenuController(Game* );
	~MenuController();
	void draw(glm::mat4) {};
	void update(double);
	Volume * getVolume() { return nullptr; }
private:
	Game * refr;

	bool choiceMade = false;
};

