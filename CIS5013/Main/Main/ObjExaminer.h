#pragma once
#include <string>
#include "tiny_obj_loader.h"
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <glm-0.9.9.2/glm/glm.hpp>
#include "Volume.h"

class ObjExaminer
{
public:
	ObjExaminer();
	~ObjExaminer();

	void setFile(std::string newFileName) { currFilename = newFileName; importPoints(); }
	void setVolume(char * volumeType) { createVolume = volumeType; }
	void setCurrShape(std::string newShape) { currShapename = newShape; sortPoints(); }

	std::string* getShapeNames();

	int getShapeCount() { return shapes.size(); }
	int getTotalVerts() { return totalVerts; }

	GLuint getVAO() { sortVAO();  return shapeVAO; }

	Volume * getVolume() { volume->createVolume(getCentre());	return volume; }

	glm::vec3 getCentre() { return averageTot; }

private:
	std::string currFilename = "";
	std::string currShapename = "";

	void importPoints();
	void sortPoints();
	void importBuffers();
	void sortVAO();

	int totalVerts = 0;

	tinyobj::attrib_t					attrib;
	std::vector<tinyobj::shape_t>		shapes;
	std::vector<tinyobj::material_t>	materials;

	GLuint vertsVBO = 0;
	GLuint textsVBO = 0;
	GLuint normsVBO = 0;
	GLuint tangentsVBO = 0;
	GLuint bitangentsVBO = 0;

	GLuint shapeVAO = 0;

	Volume * volume = nullptr;

	//The const to define size of vertexes
	static const int MAXVERTEXES = 50000;

	//Temporary container for points
	GLfloat pointsBuffer[MAXVERTEXES * 3] = { 0.0f };

	//Temporary container for normals
	GLfloat normalsBuffer[MAXVERTEXES * 3] = { 0.0f };

	//Temporary container for texture co-ords
	GLfloat textureCoordsBuffer[MAXVERTEXES * 2] = { 0.0f };
	
	//Temporary container for the tangents
	GLfloat tangentsBuffer[MAXVERTEXES*4] = {0.0f};

	//Temporary container for the bitangents
	GLfloat bitangentsBuffer[MAXVERTEXES * 4] = { 0.0f };

	float *points;
	float *norms;
	float *texts;
	float *tangents;
	float *bitangents;


	char * createVolume= "None";
	bool makingVolume = false;

	glm::vec3 averageTot = glm::vec3(0, 0, 0);
};

