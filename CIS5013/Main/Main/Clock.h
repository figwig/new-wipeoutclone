#pragma once
class Clock
{
public:
	Clock() {}
	~Clock() {}
	void tick();
	float getDelta() { return delta; }
	void end();
	int getFPS() { return avgFPS; }

private:
	double ticks = 0.0f;
	double lastTime = 0.0f;
	float delta = 0.0f;
	double frames = 0.0f;

	void delay();

	const float target = 6.94f;

	int avgFPS = 0;
};