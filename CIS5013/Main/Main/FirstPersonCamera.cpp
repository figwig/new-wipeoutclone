#include "FirstPersonCamera.h"



FirstPersonCamera::FirstPersonCamera(GameObject * newPlayer)
{
	posTrackRelRotPlayer = glm::vec3(0.0f, 0.0f, 0.0f);
	posRelPlayer = glm::vec3(0.0f, 0.0f, 0.0f);
	player = newPlayer;
	
}


FirstPersonCamera::~FirstPersonCamera()
{
}

void FirstPersonCamera::update(double deltaTimed, int mouseX, int mouseY, bool playing)
{
	static int centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
	static int centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

	if ((cameraAngle.x <= maxX) && (cameraAngle.x >= -maxX))
	{
		cameraAngle.x +=( 1.0f*(mouseY - centerY))/mouseSens;
		
	}
	else if (cameraAngle.x > maxX)
	{
		cameraAngle.x = maxX - 0.001f;
	}
	else if (cameraAngle.x <- maxX)
	{
		cameraAngle.x = -maxX + 0.001f;
	}
	

	cameraAngle.y +=( -1.0f*(mouseX - centerX))/mouseSens;

	float deltaTime = deltaTimed;

	///<Summary>
	///1. The first step calculates the new camera position
	///</Summary>

	///<Summary>
	///1.5. because this is a first person camera, rotate the player object by the y rotation of the camera
	///</Summary>
	player->setRot(glm::vec3(0, cameraAngle.y, 0));


	if (player != nullptr)
	{
		glm::vec3 totalOffset = (player->getRotMat() *  positionOffset);

		cameraPos = player->getPos() - totalOffset;
	}
	else
	{
		cameraPos = glm::vec3(0, 0, 0);
	}


	///<Summary>
	///2. The second step calculates the camera target
	///</Summary>
	glm::vec3 lookingAt = glm::rotateX(lookingAtConst, cameraAngle.x);
	lookingAt = glm::rotateY(lookingAt, cameraAngle.y);

	cameraTarget = cameraPos - lookingAt;
	//What is the camera looking at ?


	///<Summary>
	///3. The third step calculates the camera up vector. Although 0,1,0 would work in this
	///simple case, it assures that the camera stays upright whatever it is looking at
	///</Summary>

	//Get the vector the camera is looking down
	cameraDirection = glm::normalize(cameraTarget - cameraPos);

	//work out what camera right is
	cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	//work out what  camera up is
	cameraUp = glm::cross(cameraDirection, cameraRight);


	///<Summary>
	///4. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>
	camera =
		persp
		* glm::lookAt(cameraPos, cameraTarget, cameraUp);

	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);
	///<Summary>
	///5. Decay the totalY and totalX rot values
	///</Summary>

	totalYRot *= yDecay;
	totalXRot *= xDecay;





}
