#include "Camera.h"

#include "PlayerShip.h"
#include <math.h>


glm::vec3 Camera::getCameraPosTarget(bool playing, GameObject * player)
{
	//Get the position from the player
	glm::vec3 playerPos;
	if (playing)
	{
		playerPos = player->getPos();
		totalYRot = (diffX / mouseSens) + player->getRot().y;
		totalXRot = (diffY / mouseSens);
	}
	else
	{
		playerPos = glm::vec3(0, 0, 0);
		totalYRot = 0.0f;
		totalXRot = 0.0f;
	}
	//work out total rotation of the camera

	//Make the additive vector
	glm::vec3 additive = posRelPlayer;

	//rotate it in the X axis
	additive = glm::rotateX(additive, totalXRot);

	//rotate it in the Y axis
	additive = glm::rotateY(additive, totalYRot);

	//work out the targetCameraPos and return it
	return (playerPos + additive);
}