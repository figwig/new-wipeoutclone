#pragma once
#include "Object3D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
class PlayerObject :
	public Object3D
{
public:
	PlayerObject();
	~PlayerObject();
	void update(double deltaTime);
	void setDefaultPos(glm::vec3 pos) { setPos(pos); defaultPos = pos; }
	glm::vec3 getDefaultPos() { return defaultPos; }
	void incrementPos(glm::vec3 pos) { posX += pos.x; posY += pos.y; posZ += pos.z; }
	void setOffset(glm::vec3 pos) { posX = defaultPos.x + pos.x;
	posY = defaultPos.y + pos.y; posZ = defaultPos.z + pos.z; }

private:
	glm::vec3 defaultPos;
};

