#pragma once
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include <stdio.h>
#include <vector>
#include "tiny_obj_loader.h"
#include <wincodec.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include <string>
#include "Volume.h"



class VAOData
{
public:
	VAOData(GLuint newVAO, int newTotalVerts, GLuint text, GLuint spec, GLuint normal) 
	{
		shapeVAO = newVAO; totalVerts = newTotalVerts; texture = text; specMap = spec; normalMap = normal;
	}
	~VAOData() {}
	
	//fetcht the VAO
	GLuint getVAO() { return shapeVAO; }


	int getTotalVerts() { return totalVerts; }

	GLuint getTexture() { return texture; }
	GLuint getSpecMap() { return specMap; }
	GLuint getNormalMap() { return normalMap; }

	void setVolume(Volume * newVolume) { volume = newVolume; }
	Volume * getVolume()
	{
		return volume; 
	}

private:

	//The VAO to store things in
	GLuint shapeVAO = 0;

	int totalVerts = 0;


	GLuint texture = 0;
	GLuint specMap = 0;
	GLuint normalMap = 0;
	Volume * volume = nullptr;
};

