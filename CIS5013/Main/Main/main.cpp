#include "main.h"
#include "shader_setup.h"
#include "keyState.h"
#include "Config.h"




using namespace std;
using namespace CoreStructures;



int main(int argc, char* argv[]) {


	//call init on openGl
	init(argc, argv);

	shaderInit();

	thisGame = new Game(thisConfig);

	//Start the Game object
	thisGame->Start();
	

	glutMainLoop();
	

	return 0;
}

void init(int argc, char* argv[])
{
	// Initialise FreeGLUT
	glutInit(&argc, argv);

	// Setup OpenGL window properties
	glutInitContextVersion(4, 3);
	glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);


	// Create window
	glutInitWindowSize(1920, 1080);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("WipeOut");

	//What to call every frame
	glutDisplayFunc(makeFrame);

	//Call on mousedown
	glutMouseFunc(mouseDown);

	//Call on mouse Move
	glutMotionFunc(mouseMove);

	//Call on keyboard event
	glutKeyboardFunc(keyEvent);

	//and Up
	glutKeyboardUpFunc(keyUpEvent);

	glutPassiveMotionFunc(passiveMouseMove);

	// Initialise GLEW library
	glewInit();

	// Initialise OpenGL state

	// Setup colour to clear the window
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Set viewplane size
	glMatrixMode(GL_PROJECTION);

	//Perspective Projection Stuff
	GUMatrix4 P = GUMatrix4::infinitePerspectiveProjection(50.0f, 16.0f/9.0f, 0.1f);
	glLoadMatrixf((GLfloat*)&P);

	//Enable Depth Testing
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);


}

void makeFrame(void)
{
	//update the game
	thisGame->update();

	//Clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Call render on the game itself
	thisGame->render();

	//flip the display
	glutSwapBuffers();
}

void shaderInit()
{

	thisConfig.mainShaders = setupShaders(string("Shaders\\vertex_shader.shader"), string("Shaders\\fragment_shader.shader"));

	thisConfig.effectShaders1 = setupShadersGeometry(string("Shaders\\vertex_shader_effect1.shader"), string("Shaders\\geometry_shader_effect1.shader"), string("Shaders\\fragment_shader_effect1.shader"));

	thisConfig.effectShaders2 = setupShadersGeometry(string("Shaders\\vertex_shader_effect1.shader"), string("Shaders\\geometry_shader_effect2.shader"), string("Shaders\\fragment_shader_effect1.shader"));

	thisConfig.normalShaders = setupShadersGeometry(string("Shaders\\vertex_shader_normal.shader"), string("Shaders\\geometry_shader_normal.shader"), string("Shaders\\fragment_shader_normal.shader"));

	thisConfig.tangentShaders = setupShadersGeometry(string("Shaders\\vertex_shader_tangent.shader"), string("Shaders\\geometry_shader_tangent.shader"), string("Shaders\\fragment_shader_tangent.shader"));

	thisConfig.skyboxShaders = setupShaders(string("Shaders\\vertex_shader_skybox.shader"), string("Shaders\\fragment_skybox.shader"));

	thisConfig.shaders2D = setupShaders(string("Shaders\\vertex_2d.shader"), string("Shaders\\fragment_2d.shader"));

	// Get uniform location of matricies

	thisConfig.locT = glGetUniformLocation(thisConfig.mainShaders, "T");
	thisConfig.locS = glGetUniformLocation(thisConfig.mainShaders, "S");
	thisConfig.locR = glGetUniformLocation(thisConfig.mainShaders, "R");
	thisConfig.locC = glGetUniformLocation(thisConfig.mainShaders, "camera");

	thisConfig.locTe1 = glGetUniformLocation(thisConfig.effectShaders1, "T");
	thisConfig.locSe1 = glGetUniformLocation(thisConfig.effectShaders1, "S");
	thisConfig.locRe1 = glGetUniformLocation(thisConfig.effectShaders1, "R");
	thisConfig.locCe1 = glGetUniformLocation(thisConfig.effectShaders1, "camera");

	thisConfig.locTe2 = glGetUniformLocation(thisConfig.effectShaders2, "T");
	thisConfig.locSe2 = glGetUniformLocation(thisConfig.effectShaders2, "S");
	thisConfig.locRe2 = glGetUniformLocation(thisConfig.effectShaders2, "R");
	thisConfig.locCe2 = glGetUniformLocation(thisConfig.effectShaders2, "camera");

	thisConfig.locTn = glGetUniformLocation(thisConfig.normalShaders, "T");
	thisConfig.locSn = glGetUniformLocation(thisConfig.normalShaders, "S");
	thisConfig.locRn = glGetUniformLocation(thisConfig.normalShaders, "R");
	thisConfig.locCn = glGetUniformLocation(thisConfig.normalShaders, "camera");

	thisConfig.locTt = glGetUniformLocation(thisConfig.tangentShaders, "T");
	thisConfig.locSt = glGetUniformLocation(thisConfig.tangentShaders, "S");
	thisConfig.locRt = glGetUniformLocation(thisConfig.tangentShaders, "R");
	thisConfig.locCt = glGetUniformLocation(thisConfig.tangentShaders, "camera");

	thisConfig.locCs = glGetUniformLocation(thisConfig.skyboxShaders, "view");
	thisConfig.locPs = glGetUniformLocation(thisConfig.skyboxShaders, "projection");

	thisConfig.locShiny = glGetUniformLocation(thisConfig.mainShaders, "materialShininess");
	thisConfig.locLightCount = glGetUniformLocation(thisConfig.mainShaders, "numLights");
	thisConfig.locCameraPos = glGetUniformLocation(thisConfig.mainShaders, "cameraPos");

	thisConfig.e1LocTime = glGetUniformLocation(thisConfig.effectShaders1, "time");
	thisConfig.e2LocTime = glGetUniformLocation(thisConfig.effectShaders2, "time");

	thisConfig.e1LocTarget = glGetUniformLocation(thisConfig.effectShaders1, "target");
	thisConfig.e2LocTarget = glGetUniformLocation(thisConfig.effectShaders2, "target");

	thisConfig.nLocProj = glGetUniformLocation(thisConfig.normalShaders, "projection");
	thisConfig.tLocProj = glGetUniformLocation(thisConfig.tangentShaders, "projection");
	thisConfig.e1LocProj = glGetUniformLocation(thisConfig.effectShaders1, "projection");
	thisConfig.e2LocProj = glGetUniformLocation(thisConfig.effectShaders2, "projection");

	thisConfig.e1LocMag = glGetUniformLocation(thisConfig.effectShaders1, "mag");
	thisConfig.e2LocMag = glGetUniformLocation(thisConfig.effectShaders2, "mag");

	thisConfig.e1LocPosRel = glGetUniformLocation(thisConfig.effectShaders1, "posRelParent");
	thisConfig.e2LocPosRel = glGetUniformLocation(thisConfig.effectShaders2, "posRelParent");

	thisConfig.locUseNormalMap = glGetUniformLocation(thisConfig.mainShaders, "useNormalMap");

	int index = 0;

	//Work out all 120 uniform locations for the lighting engine. This is up to 10 lights, with 7 parameters each
	for (int count = 0; count < thisConfig.MAX_LIGHTS*thisConfig.PARAMS_PER_LIGHT; count++)
	{
		//if the number of params had been reached, increment the index by 1
		if (count == thisConfig.PARAMS_PER_LIGHT*(index + 1))
		{
			index++;
		}

		//fill the array of lighting uniforms using the method below
		thisConfig.lightingUniforms[count] = getLightUniformLocationName(count - (index*thisConfig.PARAMS_PER_LIGHT), index);
	}

}

GLuint getLightUniformLocationName(int count, int index)
{
	//work out the name of the property based on index and the propertyname
	std::ostringstream ss;
	ss << "allLights[" << index << "]." << propertyNames[count];
	std::string s = ss.str();
	const char * uniformName = s.c_str();

	//return the unifrom location
	return glGetUniformLocation(thisConfig.mainShaders, uniformName);
}



#pragma region GameCalls

void mouseDown(int buttonID, int state, int x, int y)
{
	thisGame->mouseDown(buttonID, state, x, y);
}

void mouseMove(int x, int y)
{
	thisGame->mouseMove(x, y);
}
void passiveMouseMove(int x, int y)
{
	thisGame->mousePassiveMouse(x, y);
}

void keyEvent(unsigned char Key, int x, int y)
{
	switch (Key)
	{
	case 27:
		exit(0);
		break;
	default:
		thisGame->keyEvent(Key, x, y, true);

		break;
	}

}

void keyUpEvent(unsigned char Key, int x, int y)
{
	thisGame->keyEvent(Key, x, y, false);
}

#pragma endregion

