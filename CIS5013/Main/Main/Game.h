#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "GameObject.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include "keyState.h"
#include "Scene.h"
#include "Config.h"
#include "ObjExaminer.h"
#include "Clock.h"
#include "Camera.h"
#include "LightingState.h"
#include "Light.h"
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>


class Custom;

class Game
{
public:
	Game(Config newConfig);
	~Game();

	void Start();
	void render(void);
	void update(void);
	void setConfigAll();

	void addGameObject(GameObject*);
	void addTransparentGameObject(GameObject*);

	LightingState* getLighting() { return thisConfig.currLighting; }
	void setLighting(LightingState* newLighting) {
		thisConfig.currLighting = newLighting; }

	void setCamera(Camera *newCamera) { gameCamera = newCamera; }
	void setPlaying(bool tf) { playing = tf; }


	Config getConfig() { return thisConfig; }

	void setMouseMode(char* mode)
	{
		if (mode == "Normal")
		{
			captureMouse = false;
		}
		else if (mode == "FirstPerson")
		{
			printf("Mouse Mode changed\n");
			captureMouse = true;
			mouseOnHold  = false;
			glutSetCursor(GLUT_CURSOR_NONE);
		}
	}

#pragma region Input Callbacks
	void mouseDown(int buttonID, int state, int x, int y);
	void mouseMove(int x, int y);
	void keyEvent(unsigned char Key, int x, int y, bool upDown);
	void mousePassiveMouse(int,int);
#pragma endregion

	Keystate getKeys();

	void setPlayer(GameObject* newPlayer) { player = newPlayer; }
	Config thisConfig;

#pragma region Scene Stuff
	void runNextScene()
	{
		currScene++;

		scenes[currScene]->run();

		setConfigAll();

		//9alSourcePlay(source1);

		delete scenes[currScene-1];
	}

	void loadScene(char * name)
	{
		for (int i = 0; i < MAX_SCENES; i++)
		{
			if (scenes[i] != nullptr)
			{
				if (scenes[i]->name == name)
				{	
					//Free up the current Scene
					delete scenes[currScene];
					scenes[currScene] = nullptr;

					//Reassign the current scene 
					currScene = i;

					//Load this next Scene (please move into a thread)
					setConfigAll();
					scenes[i]->load(this);
					scenes[i]->run();
					return;
				}
			}
		}
		printf("Scene Not Found");
	}

	void loadNextScene()
	{
		scenes[currScene+1]->load(this);
		runNextScene();
	}
#pragma endregion

private:

#pragma region GameObjects and Arrays of Gameobjects
	//The Pointer to the player
	GameObject* player = nullptr;
#pragma endregion

#pragma region Object Containers
	//The keyState
	Keystate keys = 0;

	//The current game clock
	Clock* gameClock = nullptr;
	//The Main camera
	Camera * gameCamera = nullptr;
	//The audio Device and context
	ALCdevice* currAudioDevice = nullptr;
	ALCcontext * currContext = nullptr;
	//Scene Management stuff
	const static int MAX_SCENES = 5;
	Scene * scenes[MAX_SCENES] = { nullptr };
	int currScene = 0;

#pragma endregion

#pragma region Sounds
	ALuint sound1;

	ALuint source1;


#pragma endregion

#pragma region bools
	bool mouseDownBool = false;
	bool playing = false;
	bool captureMouse = false;
	bool mouseOnHold = true;
#pragma endregion

#pragma region floats
	float mousePrevX, mousePrevY;
	float deltaTime = 0;
	float diffX = 0.0f;
	float diffY = 0.0f;
#pragma endregion

	glm::mat4 cameraMatrix;

};

