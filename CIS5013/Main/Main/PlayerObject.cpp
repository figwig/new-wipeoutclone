#include "PlayerObject.h"



PlayerObject::PlayerObject()
{
	scaleX = 0.5;
	scaleY = 0.5;
	scaleZ = 0.5;
}


PlayerObject::~PlayerObject()
{
}

void PlayerObject::update(double deltaTime)
{
	Object3D::update(deltaTime);
	Object3D::lateUpdate(deltaTime);
}
