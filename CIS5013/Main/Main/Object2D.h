#pragma once
#include "GameObject.h"
#include <stdio.h>
#include <string>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include <glew\glew.h>
#include <freeglut\freeglut.h>

class Object2D : public GameObject
{
public:
	
	Object2D();
	~Object2D() {}
	void draw(glm::mat4 camera) {}
	void update(double delta) { deltaTime = delta; }
	Volume * getVolume() { return nullptr; }
protected:

	//The filename container
	std::string fileName = "Textures/load.png";

	//Texture containers
	GLuint texture = 0;
	char* textureFileName = "";

	double deltaTime;

};