#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include "GameObject.h"

class Light : public GameObject {
public:
	Light() { }

	int id = 0;

	void update(double){}
	void draw(glm::mat4) {}

	void setRot(glm::vec3 newRot)
	{
		GameObject::setRot(newRot);
		coneDirection = GameObject::getRotMat() * glm::vec4(coneDirection,0);
	}

	glm::vec4 position;
	glm::vec3 intensities;
	float attenuation;
	float ambientCoefficient;
	float coneAngle; 
	glm::vec3 coneDirection; 
	Volume * getVolume() { return nullptr; }


	glm::mat4 finalMatrix = glm::mat4(1);
};