#pragma once
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include "LightingState.h"

struct Config 
{
	LightingState * currLighting;

#pragma region Uniform Locations
	GLuint locT;
	GLuint locTe1;
	GLuint locTe2;
	GLuint locTn;
	GLuint locTt;

	GLuint locR;
	GLuint locRe1;
	GLuint locRe2;
	GLuint locRn;
	GLuint locRt;

	GLuint locS;
	GLuint locSe1;
	GLuint locSe2;
	GLuint locSn;
	GLuint locSt;

	GLuint locC;
	GLuint locCe1; 
	GLuint locCe2; 
	GLuint locCn;
	GLuint locCt;
	GLuint locCs;

	GLuint locPs;

	GLuint e2LocTime;
	GLuint e1LocTime;
	GLuint e2LocTarget;
	GLuint e1LocTarget;

	GLuint e1LocProj;
	GLuint e2LocProj;
	GLuint nLocProj;
	GLuint tLocProj;
	GLuint e1LocMag;
	GLuint e2LocMag;

	GLuint e1LocPosRel;
	GLuint e2LocPosRel;

	GLuint locShiny;
	GLuint locLightCount;
	GLuint locCameraPos;

	GLuint locUseNormalMap;

	GLuint mainShaders;
	GLuint effectShaders1;
	GLuint effectShaders2;
	GLuint normalShaders;
	GLuint tangentShaders;
	GLuint skyboxShaders;
	GLuint shaders2D;

	const static int MAX_LIGHTS = 12;
	const static int PARAMS_PER_LIGHT = 7;

	GLuint lightingUniforms[MAX_LIGHTS * PARAMS_PER_LIGHT];
#pragma endregion

};