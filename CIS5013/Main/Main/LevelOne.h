#pragma once
#include "Scene.h"
#include "Game.h"
#include "VAOData.h"
#include "ObjExaminer.h"
#include "PlayerShip.h"
#include "GameObject.h"

class LevelOne :
	public Scene
{
public:
	LevelOne();
	~LevelOne();

	void run();
	void load(Game* thisGame);


#pragma region Load Calls
	void loadTextures();
	void loadWorld();
	void loadPlayer();


#pragma endregion

private:
	PlayerShip * player;

	GLuint ship = 0;
	GLuint lamppost = 0;



#pragma region Textures
	GLuint metal = 0;
	GLuint redMetal = 0;
	GLuint shipTex = 0;
	GLuint shipSpec = 0;
	GLuint asphalt = 0;
	GLuint asphaltSpec = 0;
	GLuint asphaltNormal = 0;
	GLuint concrete = 0;
	GLuint road = 0;
	GLuint billBoard = 0;
#pragma endregion

};

