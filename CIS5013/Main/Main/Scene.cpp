#include "Scene.h"
#include <stdio.h>



void Scene::addGameObject(GameObject * newObject)
{
	int count = 0;

	for (count; count < RENDERABLE_MAX; count++)
	{
		if (renderables[count] == nullptr)
		{
			renderables[count] = newObject;
			renderCount++;
			return;
		}
	}
}

void Scene::addTransparentGameObject(GameObject * newObject)
{
	int count = 0;

	for (count; count < RENDERABLE_TRANS_MAX; count++)
	{
		if (renderablesTrans[count] == nullptr)
		{
			renderablesTrans[count] = newObject;
			transRenderCount++;
			return;
		}
	}
}
