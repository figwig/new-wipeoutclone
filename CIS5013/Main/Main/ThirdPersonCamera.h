#pragma once
#include "Camera.h"
class ThirdPersonCamera :
	public Camera
{
public:
	ThirdPersonCamera();
	~ThirdPersonCamera();
	void update(double deltaTimed, int mouseX, int mouseY, bool playing);
};

