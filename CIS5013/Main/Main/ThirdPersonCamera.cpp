#include "ThirdPersonCamera.h"



ThirdPersonCamera::ThirdPersonCamera()
{
}


ThirdPersonCamera::~ThirdPersonCamera()
{
}


void ThirdPersonCamera::update(double deltaTimed, int mouseX, int mouseY, bool playing)
{
	float deltaTime = deltaTimed;
	diffX = mouseX;
	diffY = mouseY;
	///<Summary>
	///1. The first step calculates the new camera position
	///</Summary>

	//What does the camera want to move towards?
	targetCameraPos = getCameraPosTarget(playing, target);

	//The vector between the desired position and current
	difference = targetCameraPos - cameraPos;


	cameraPos += ((cameraSpeed)* difference);
	//cameraPos = targetCameraPos;

	///<Summary>
	///2. The second step calculates the camera target
	///</Summary>

	if (playing)
	{
		cameraTarget = target->getPos() + glm::rotateY(posTrackRelRotPlayer, target->getRot().y);
	}
	else
	{
		cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
	}
	//What is the camera looking at ?


	///<Summary>
	///3. The third step calculates the camera up vector. Although 0,1,0 would work in this
	///simple case, it assures that the camera stays upright whatever it is looking at
	///</Summary>

	//Get the vector the camera is looking down
	cameraDirection = glm::normalize(cameraTarget - cameraPos);

	//work out what camera right is
	cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	//work out what  camera up is
	cameraUp = glm::cross(cameraDirection, cameraRight);


	///<Summary>
	///4. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>
	camera =
		persp
		* glm::lookAt(cameraPos, cameraTarget, cameraUp);

	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);
	///<Summary>
	///5. Decay the totalY and totalX rot values
	///</Summary>
	totalYRot *= yDecay;
	totalXRot *= xDecay;
}
