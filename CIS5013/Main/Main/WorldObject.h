#pragma once
#include "Object3D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
class WorldObject :
	public Object3D
{
public:
	WorldObject(VAOData* shape);
	~WorldObject();
	void update(double deltaTime);
	void draw(glm::mat4);
};

