#include "MenuController.h"



MenuController::MenuController(Game* newRefr)
{
	refr = newRefr;
}


MenuController::~MenuController()
{
}

void MenuController::update(double dt)
{
	if (!choiceMade)
	{
		Keystate keys = refr->getKeys();

		if (keys & Keys::num1)
		{	
			printf("Loading scene...\n");
			refr->loadScene("WipeOut");
			choiceMade = true;
		}
		if (keys & Keys::num2)
		{
			printf("Loading scene...\n");
			refr->loadScene("Night");
			choiceMade = true;
		}
	}

}