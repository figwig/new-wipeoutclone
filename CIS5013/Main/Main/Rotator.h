#pragma once
#include "GameObject.h"
class Rotator :
	public GameObject
{
public:
	Rotator(GameObject* newTarget, float newSpeed) { target = newTarget; speed = newSpeed; }
	~Rotator();

	void update(double delta) { target->setRot(glm::vec3(target->getRot().x, target->getRot().y + speed * delta, target->getRot().z)); }
	void draw(glm::mat4) {}
	Volume * getVolume() { return nullptr; }
private:
	GameObject * target;
	float speed = 0.0f;
};

