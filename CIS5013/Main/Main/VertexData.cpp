#include "VertexData.h"
#include "Point3f.h"

float * VertexData::getVertsf()
{	
	if (!vertsGot) 
	{
		for (int count = 0; count < vertCount; count++)
		{
			points[3 * count] = vertexes[count]->x;
			points[(3 * count) + 1] = vertexes[count]->y;
			points[(3 * count) + 2] = vertexes[count]->z;
		}
		vertsGot = true;
	}
	else
	{
		//Already generated
	}


	return points;
}


float * VertexData::getNormsf()
{
	if (!normsGot)
	{
		for (int count = 0; count < vertCount; count++)
		{
			normalsf[3 * count] = normals[count]->x;
			normalsf[(3 * count) + 1] = normals[count]->y;
			normalsf[(3 * count) + 2] = normals[count]->z;
		}
		normsGot = true;
	}
	else
	{
		//already generated
	}
	return normalsf;
}


float * VertexData::getTexts2f()
{
	if (!textsGot)
	{
		for (int count = 0; count < vertCount; count++)
		{
			textsf[2 * count] = texts[count]->x;
			textsf[(2 * count) + 1] = texts[count]->y;
			
		}
		textsGot = true;
	}
	else
	{
		//texts have already been generated
	}


	return textsf;
}

void VertexData::addVertex(Point3f* newVertex, int index)
{
	vertexes[index] = newVertex;

}

void VertexData::removeVertex(Point3f vertext)
{

}
void VertexData::removeVertex(int index)
{
	
}

void VertexData::addVertexes(Point3f* newVertexes[], int size)
{
	vertCount = size;
	for (int count = 0; count < size; count++)
	{
		vertexes[count] = newVertexes[count];
	}

}

void VertexData::addNormals(Point3f* newNormals[], int size)
{
	for (int count = 0; count < size; count++)
	{
		normals[count] = newNormals[count];
	}

}

void VertexData::addTextUvs(Point2f * newTexts[], int size)
{

	for (int count = 0; count < size; count++)
	{
		texts[count] = newTexts[count];
	}
}


