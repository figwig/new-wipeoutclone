#include "LiteralVolume.h"
#include "GameObject.h"
#include <iostream>


ConvexVol::ConvexVol()
{

}


ConvexVol::~ConvexVol()
{
}

void ConvexVol::createVolume(glm::vec3 newCentre)
{
	centre = newCentre;
	//Because literal volume has the same number and position for each of it's check points, this method is very simple
	checkPoints = new glm::vec3[bufferIndex];

	for (int count = 0; count < bufferIndex; count++)
	{
		checkPoints[count] = checkBufferPoints[count];

	}
}

bool ConvexVol::checkPoint(glm::vec3 point)
{
	glm::mat4 currMat = owner->getTransMat() * owner->getRotMat();




	return false; 
}

bool ConvexVol::checkVolume(Volume * visitor)
{
	//New Algorithm --
	//Step 1. Find the point in the visitor volume that is closest to this volume's centre
	glm::vec3 thisPoint;
	//the default default value for the closest
	glm::vec3 vectorToClosestPointFromCentre = visitor->getCentre()-centre;

	for (int count = 0; count < visitor->getPointsCount(); count++)
	{
		//What point are we testing?
		thisPoint = visitor->getPointByIndex(count);

		//What is smaller, the existing vector or the vector between the new point and the centre
		vectorToClosestPointFromCentre = glm::min(thisPoint- centre, vectorToClosestPointFromCentre- centre);

	}
	glm::vec3 closestPointToCentre = vectorToClosestPointFromCentre + centre;
	//Find the point in this volume closest to the point found above, then test it

	//For more accuracy, find the point from the visitor volume closest to the point selected on this volume

	return false;

}