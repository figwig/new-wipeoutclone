#pragma once
#include "ObjExaminer.h"
#include "GameObject.h"
#include "Config.h"
#include "LightingState.h"

class Game;

class Scene
{
public:
	Scene() {}
	~Scene() {}
	char * name;
	//There are some requirments for a scene to run in the next 3 methods
	virtual void run() = 0;
	//Run HAS to set the playing state, and the player pointer
	virtual void load(Game* thisGame) = 0;
	//load has to call loadtextures, and set the lighting state of the game
	virtual void loadTextures() = 0;

	int getNumObjects() { return renderCount; }
	int getNumTransObjects() { return transRenderCount; }
	GameObject* getObjectByIndex(int i) { return renderables[i]; }
	GameObject* getTransObjectByIndex(int i) { return renderablesTrans[i]; }

	void addGameObject(GameObject*);
	void addTransparentGameObject(GameObject*);

	LightingState * currLighting;
protected:
	Game * gameRefr;

	ObjExaminer * examiner;

	int renderCount = 0;
	int transRenderCount = 0;

	// Renderable objects
	static const int RENDERABLE_MAX = 60;
	GameObject* renderables[RENDERABLE_MAX] = { nullptr };

	// Renderable Transparent Objects
	static const int RENDERABLE_TRANS_MAX = 35;
	GameObject* renderablesTrans[RENDERABLE_TRANS_MAX] = { nullptr };

};

