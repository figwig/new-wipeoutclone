#include "NightScene.h"
#include "Game.h"
#include "VAOData.h"
#include "Scenery.h"
#include "MoonLight.h"
#include "FirstPersonCamera.h"
#include "Skybox.h"
#include "smallSpotLight.h"
#include "PlayerController.h"
#include "Rotator.h"
#include "PointLight.h"
#include "Volume.h"

NightScene::NightScene()
{
	name = "Night";
}


NightScene::~NightScene()
{
	delete currLighting;
}

void NightScene::run()
{
	gameRefr->setMouseMode("FirstPerson");
	gameRefr->setPlayer(player);
	gameRefr->setPlaying(true);
}
void NightScene::loadTextures()
{
	asphalt = fiLoadTexture("Textures/Asphalt.png");
	asphaltSpec = fiLoadTexture("Textures/AsphaltSpec.png");
	asphaltNormal = fiLoadTexture("Textures/AsphaltNormal.png");
	metal = fiLoadTexture("Textures/metal.jpg");
	brickWall = fiLoadTexture("Textures/brickwall.jpg");
	brickWallNormal = fiLoadTexture("Textures/brickwall_normal.jpg");

	shipSpec = fiLoadTexture("Textures/shipSpec.png");
}
void NightScene::load(Game* thisGame) 
{
	gameRefr = thisGame;

	static int centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
	static int centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;

	glutWarpPointer(centerX/2, centerY/2);

	GameObject * skyBox = new Skybox(1);

	addGameObject(skyBox);

	currLighting = new LightingState();

	gameRefr->thisConfig.currLighting = currLighting;

	loadTextures();

	examiner = new ObjExaminer();

//Add a Cube to the scene
	//Set the file
	examiner->setFile("Objects/Cube.obj");

	//set the volume type
	examiner->setVolume("Literal");

	//set the shape name
	examiner->setCurrShape("Master");

	//get the volume from the examiner
	Volume * cubeVol = examiner->getVolume();

	//Reset the volume type
	examiner->setVolume("None");

	//Get the VAO
	GLuint cubeVAO = examiner->getVAO();
	
	//get the cube size 
	int cubeSize = examiner->getTotalVerts();

	//Create a VAO data from this
	VAOData *cube = new VAOData(cubeVAO, cubeSize, brickWall, asphaltSpec, brickWallNormal);
	//and set it's volume
	cube->setVolume(cubeVol);

	examiner->setFile("Objects/Cylinder.obj");

	examiner->setCurrShape("Master");

	GLuint cylinderVAO = examiner->getVAO();

	int cylinderSize = examiner->getTotalVerts();

	VAOData * cylinder = new VAOData(cylinderVAO, cylinderSize, brickWall, asphaltSpec, brickWallNormal);

//Make two cubes from the VAO stuff at different positions
	Custom *cube1 = new Custom(cube, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.1f, 0.1f, 0.1f), glm::vec3(0.0f, 0.0f, 0.0f));

	Custom *cube2 = new Custom(cube, glm::vec3(0.0f, 0.0f, -30.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f));

	Custom *cube3 = new Custom(cylinder, glm::vec3(0.0f, 14.8f, -33.0f), glm::vec3(0.05f, 0.05f, 0.05f), glm::vec3(0.0f, 0.0f, 0.0f));
//Add both cubes to the scene
	addGameObject(cube1);
	addGameObject(cube3);

	cube3->drawTangent = true;
	//cube3->drawNormal =	 true;

	addGameObject(cube2);

	//cube1 is the player
	player = cube1;

//Make a player controller
	PlayerController * controller = new PlayerController(gameRefr);

	controller->setTarget(player);

	addGameObject(controller);


//set up a rotator for the second cube
	Rotator *cubeRotator = new Rotator(cube3, 0.0003f);

	addGameObject(cubeRotator);

//Setup the game camera
	gameRefr->setCamera(new FirstPersonCamera(player));



//////
//LIGHTING
//////


//Add a basic moonlight to the scene

	currLighting->addLight(new Moonlight(glm::vec3(0.0, 0.8f, 0.8f)));

	smallSpotLight * spotLight = new smallSpotLight(glm::vec3(0.0, 0.7f, 0.0f), glm::vec3(0.0f, 17.0f, -25.0f));
	currLighting->addLight(spotLight);
	spotLight->coneDirection = glm::rotateX(spotLight->coneDirection, 0.9f);

	PointLight* pointLight = new PointLight(glm::vec3(0.0f, 20.0f, -30.0f));
	//currLighting->addLight(pointLight);

//Add the floating city
	examiner->setFile("Objects/FloatCity.obj");

	examiner->setCurrShape("City");


	Custom *floatingCity = new Custom(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), metal, 0, 0),

		glm::vec3(10, 0, -100.0f),
		glm::vec3(10, 10, 10),
		glm::vec3(0, 0, 0));

	//addGameObject(floatingCity);

	//floatingCity->drawTangent = true;
	gameRefr->setConfigAll();
}
