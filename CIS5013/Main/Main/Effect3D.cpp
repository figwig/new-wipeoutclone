#include "Effect3D.h"
#include <CoreStructures/CoreStructures.h>
#include <vector>

using namespace CoreStructures;

Effect3D::Effect3D()
{

}
Effect3D::~Effect3D()
{

}

void Effect3D::update(double newdeltaTime)
{
	deltaTime = newdeltaTime;

	if (parent != nullptr)
	{
		//Set up matricies representing the parent
		pR = glm::toMat4(
			glm::quat(glm::vec3(0, parent->getRot().y, 0))*
			glm::quat(glm::vec3(parent->getRot().x, 0, 0))*
			glm::quat(glm::vec3(0, 0, parent->getRot().z))
		);

		pT = GUMatrix4::translationMatrix(parent->getPos().x, parent->getPos().y, parent->getPos().z);

		//Set up matricies representing overall transform and rotation
		R = pR;
		T = pT;
		
	}
	else
	{
		R = glm::toMat4(
			glm::quat(glm::vec3(0.0f, thetaY, 0.0f))*
			glm::quat(glm::vec3(thetaX, 0.0f, 0.0f))*
			glm::quat(glm::vec3(0.0f, 0.0f, thetaZ)));
		T = GUMatrix4::translationMatrix(posX, posY, posZ);
		S = GUMatrix4::scaleMatrix(scaleX, scaleY, scaleZ);
	}
}



