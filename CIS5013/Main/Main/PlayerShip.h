#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "texture_loader.h"
#include "Object3D.h"
#include "VertexData.h"
#include <stdio.h>
#include "Effect3D.h"
#include "PlayerObject.h"
#include "EngineBlue.h"
#include "ObjExaminer.h"
#include "VAOData.h"
#include "HoverJet.h"
#include "Light.h"

//A struct to contain all of the parts that form the ship as well as the core ship model
struct PlayerShipComponents
{
	Effect3D * leftEngineEffect = nullptr;
	Effect3D * insideleftEngineEffect = nullptr;
	Effect3D * rightEngineEffect = nullptr;
	Effect3D * insiderightEngineEffect = nullptr;
	PlayerObject * leftEngineCone = nullptr;
	PlayerObject * rightEngineCone = nullptr;
	PlayerObject * canopy = nullptr;
	PlayerObject * hoverJets[10] = { nullptr };
	HoverJet * hoverJetEffects[10] = { nullptr };
	Light * headLight = nullptr;
};

class Game; 
using namespace CoreStructures;

class PlayerShip : public Object3D
{
public:
	PlayerShip(Game* gameRefr, VAOData* newData);
	~PlayerShip();
	void update(double deltaTime);
	void setComponents(PlayerShipComponents newParts) {shipParts = newParts;}

	void initParts();

	PlayerShipComponents shipParts;

private:
	//The player's reference the main game object to poll it for input data and other things
	Game * refr;

	//the method that polls the main game object
	void checkInput();

	void draw(glm::mat4 camera);

	void updateEffects();

	float intensityL = 0.0f;
	float intensityR = 0.0f;


#pragma region Movement constants
	const float acceleration = 0.0015f;
	const float rotAcceleration = 0.000017f;
	const float vertAcceleration = 0.00025f;

	const float maxSpeed =27.0f;
	const float maxRotSpeed = 0.04f;
	const float maxVertSpeed = 4.0f;

	const float drag = 0.0018f;
	const float rotDrag = 0.994f;
	const float xRotDecay = 0.99f;
	const float fixedDecay = 0.0008f;

	const float rotTiltThreshold = 0.028f;
	const float maxTilt = 0.35f;
	const float xDecayReductionPerFrame = 0.3f;

#pragma endregion

#pragma region Movement Methods and containers
	//The total movement to be applied at the end of the frame
	glm::vec3 movement = glm::vec3(0.0f, 0.0f, 0.0f);

	//The total rotation to be applied at the end of the frame
	glm::vec3 rotation = glm::vec3(0.0f, 0.0f, 0.0f);

	//Apply a vector to the total movement to happen this frame
	void applyAcceleration(glm::vec3 vector);
	void applyRotation(glm::vec3 vector);
	void applyVertAcceleration(glm::vec3 vector);

	//Move using the total movement needed this frame
	void move();

	void decayMomentum();
	void decayMomentumRot();
	void tendToFlat();

	void cornerRotate();

	float tilt = 0.0f;

	float speed = 0.0f;
	float rotSpeed = 0.0f;

#pragma endregion

#pragma region Non-Movement Consts
	float baseEngineIntensity = 1.8f;
	float turningEngineIntensityBase = 0.0025f;
	float turningEngineIntensityDecay = 0.99f;
	float multiplictiveFactorAddition = 0.019f;
	float minimumEngineEffectThreshold = 0.015f;
	float engineConeMovementMultiplier = 1.2f;
	float insideEngineConeReduction = 0.95f;

	glm::vec4 headLightPos=  glm::vec4(0.0f, 0.0f, -24.0f, 1.0f);
#pragma endregion

};
