#pragma once
#include "Config.h"
#include "Object2D.h"

class ImageRect :
	public Object2D
{
public:
	ImageRect( char* newTextFileName);
	~ImageRect();
	void sortVAO();
	void draw(glm::mat4 camera);
	void setScale(int nx) { scaleX = nx; applyScaling(); }

protected:
	const int totalPoints = 8;

	float points[8] =
	{
		-1.0f,1.0f,
		1.0f,1.0f,
		1.0f,-1.0f,
		-1.0f,-1.0f
	};	
	float texts[8] =
	{
		0.0,1.0f,
		1.0f,1.0f,
		1.0f,0.0f,
		0.0f,0.0f
	};

	float scaleX;

	GLuint VAO = 0;

	GLuint pointsVBO = 0;
	GLuint textsVBO = 0;
	void applyScaling();

};

