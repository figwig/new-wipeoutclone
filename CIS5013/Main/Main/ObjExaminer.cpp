#include "ObjExaminer.h"
#include <iostream>
#include <math.h>
#include "LiteralVolume.h"
#include <glm-0.9.9.2/glm/glm.hpp>

using namespace tinyobj;

ObjExaminer::ObjExaminer()
{
}

ObjExaminer::~ObjExaminer()
{
}

std::string * ObjExaminer::getShapeNames()
{
	return nullptr;
}

void ObjExaminer::importPoints()
{

	//a container for any error that might happen
	std::string err;

	//Load the object into the right containers
	bool result = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, currFilename.c_str());

	//if err isn't empty, print the string
	if (!err.empty()) {
		std::cerr << err << std::endl;

	}
}

void ObjExaminer::sortPoints()
{
	//Since this class is a singleton, reset all variables used
	totalVerts = 0;

	averageTot = glm::vec3(0, 0, 0);

	int count = 0;

	//Variables used for the tangent calculations
	glm::vec3 v0;
	glm::vec3 v1;
	glm::vec3 v2;

	glm::vec2 w0;
	glm::vec2 w1;
	glm::vec2 w2;

	glm::vec3 deltaPos1;
	glm::vec3 deltaPos2;

	glm::vec2 deltaUV1;
	glm::vec2 deltaUV2;

	glm::vec3 tangent;
	glm::vec3 bitangent;

	if (createVolume == "None")
	{
		makingVolume = false;
	}
	else if (createVolume == "Literal")
	{
		volume = new ConvexVol();
		makingVolume = true;
	}

	int triCount = 0;
	//Looping through shapes, faces, then vertexes
	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;
		
		//If the shape name is master or fits the current shape name, proceed to import the points
		if ((currShapename == "Master") || currShapename == shapes[s].name)
		{

			for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++)
			{


				//For each Face

				int fv = shapes[s].mesh.num_face_vertices[f];

				// Loop over triangles in the face
				for (size_t v = 0; v < fv; v++) {
					//this section of code is PER VERTEX

					//Add each vertex, normal and tex coord to the temp containers
#pragma region Buffer Assignment
					index_t idx = shapes[s].mesh.indices[index_offset + v];

					//Normals
					real_t nx = attrib.normals[3 * idx.normal_index + 0];
					real_t ny = attrib.normals[3 * idx.normal_index + 1];
					real_t nz = attrib.normals[3 * idx.normal_index + 2];

					normalsBuffer[count * 3] = nx;
					normalsBuffer[(count * 3) + 1] = ny;
					normalsBuffer[(count * 3) + 2] = nz;


					//vertexes
					real_t vx = attrib.vertices[3 * idx.vertex_index + 0];
					real_t vy = attrib.vertices[3 * idx.vertex_index + 1];
					real_t vz = attrib.vertices[3 * idx.vertex_index + 2];
					

					pointsBuffer[count * 3] = vx;
					pointsBuffer[(count * 3) + 1] = vy;
					pointsBuffer[(count * 3) + 2] = vz;

					//Texture Co-ords
					real_t tx = attrib.texcoords[2 * idx.texcoord_index + 0];
					real_t ty = attrib.texcoords[2 * idx.texcoord_index + 1];

					textureCoordsBuffer[count * 2] = tx;
					textureCoordsBuffer[(count * 2) + 1] = ty;

					//Add to the average pos 
					averageTot += glm::vec3(vx, vy, vz);
					count += 1;
#pragma endregion
					//if this shape needs a volume made for it
					if (makingVolume)
					{
						volume->addBufferPoint(glm::vec3(vx, vy, vz));
					}
					//Get the vertex and texture coord vectors for tangent calculations, aka positions and uvs for the three points
					if (v == 0)
					{
						v0 = glm::vec3(vx, vy, vz);
						w0 = glm::vec2(tx, ty);
					}
					else if (v == 1)
					{
						v1 = glm::vec3(vx, vy, vz);
						w1 = glm::vec2(tx, ty);
					}
					else
					{
						v2 = glm::vec3(vx, vy, vz);
						w2 = glm::vec2(tx, ty);
					}
				}
				//Calculate Tangent for each point


				//This section of code is PER TRIANGLE. Count is at the first vertex of the next triangle.
				if (true)//potential to turn off tangent calculations
				{
					deltaPos1 = v1 - v0;
					deltaPos2 = v2 - v0;

					deltaUV1 = w1 - w0;
					deltaUV2 = w2 - w0;
					
					float r = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
					tangent = (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y)*r;
					bitangent = bitangent = (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x)*r;
				}

				tangentsBuffer[(triCount*12)] = tangent.x;
				tangentsBuffer[(triCount*12)+1] = tangent.y;
				tangentsBuffer[(triCount*12)+2] = tangent.z;
				tangentsBuffer[(triCount*12)+3] = 1.0;

				tangentsBuffer[(triCount * 12)+4] = tangent.x;
				tangentsBuffer[(triCount * 12) + 5] = tangent.y;
				tangentsBuffer[(triCount * 12) + 6] = tangent.z;
				tangentsBuffer[(triCount * 12) + 7] = 1.0;

				tangentsBuffer[(triCount * 12) + 8] = tangent.x;
				tangentsBuffer[(triCount * 12) + 9] = tangent.y;
				tangentsBuffer[(triCount * 12) + 10] = tangent.z;
				tangentsBuffer[(triCount * 12) + 11] = 1.0;


				bitangentsBuffer[(triCount * 12)] = bitangent.x;
				bitangentsBuffer[(triCount * 12) + 1] = bitangent.y;
				bitangentsBuffer[(triCount * 12) + 2] = bitangent.z;
				bitangentsBuffer[(triCount * 12) + 3] = 1.0;

				bitangentsBuffer[(triCount * 12) + 4] = bitangent.x;
				bitangentsBuffer[(triCount * 12) + 5] = bitangent.y;
				bitangentsBuffer[(triCount * 12) + 6] = bitangent.z;
				bitangentsBuffer[(triCount * 12) + 7] = 1.0;

				bitangentsBuffer[(triCount * 12) + 8] = bitangent.x;
				bitangentsBuffer[(triCount * 12) + 9] = bitangent.y;
				bitangentsBuffer[(triCount * 12) + 10] = bitangent.z;
				bitangentsBuffer[(triCount * 12) + 11] = 1.0;

				index_offset += fv;

				// per-face material
				shapes[s].mesh.material_ids[f];

				triCount++;
			}
		}

	}
	//work out the average point aka centre
	averageTot /= count;
	

	totalVerts = count;


	importBuffers();
}

void ObjExaminer::importBuffers()
{
	//Set up dynamic arrays for properly sized VAOs.
	//points and normals have 3 floats per vertex
	points = new float[totalVerts * 3];
	norms = new float[totalVerts * 3];

	//the tangent has 4 floats per vertex
	tangents = new float[totalVerts * 4];
	bitangents = new float[totalVerts * 4];

	//the texture coord is 2 floats per vertex
	texts = new float[totalVerts * 2];


	//Fill the dynamically allocated arrays with the numbers from the buffers
	for (int i = 0; i < totalVerts * 3; i++)
	{
		points[i] = pointsBuffer[i];
		norms[i] = normalsBuffer[i];
	}

	//and the text coords
	for (int i = 0; i < totalVerts * 2; i++)
	{
		texts[i] = textureCoordsBuffer[i];
	}

	//once for each vertex
	for (int i = 0; i < totalVerts* 4; i++)
	{
		tangents[i] = tangentsBuffer[i];
		bitangents[i] = bitangentsBuffer[i];
	}

	

}

void ObjExaminer::sortVAO()
{
	vertsVBO = 0;
	normsVBO = 0;
	textsVBO = 0;

	shapeVAO = 0;

	//Make the VAO
	glGenVertexArrays(1, &shapeVAO);
	glBindVertexArray(shapeVAO);


	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*3, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);


	//The normal Buffer
	glGenBuffers(1, &normsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, normsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*3, norms, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	//the tangent buffer
	glGenBuffers(1, &tangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, tangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts * 4, tangents, GL_STATIC_DRAW);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(3);

	glGenBuffers(1, &bitangentsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bitangentsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts * 4, bitangents, GL_STATIC_DRAW);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(4);

	//and the texture co-ord buffer
	glGenBuffers(1, &textsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, textsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float*)*totalVerts*2, texts, GL_STATIC_DRAW);
	glVertexAttribPointer(8, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(8);

	//Unbind VAO
	glBindVertexArray(0);

	delete [] points;
	delete [] norms;
	delete [] texts;
	delete [] tangents;
}
