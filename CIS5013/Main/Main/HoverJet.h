#pragma once
#include "Effect3D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include <random>

class HoverJet : public Effect3D
{
public:
	HoverJet();
	~HoverJet();
	void draw(glm::mat4);
	void setRadius(float x) { radius = x; }

protected:

	void generateCircle(int noPoints, float radius);
	void generateSecondaryCircle(int noPoints, float radius);
	void shaderUniformSetup(glm::mat4 camera);
	void sortVAO();
	void sortVAO2();


	bool firstCircle = true;

	GLuint VAO2;
	int totalPoints2 = 0;

	GLuint VBO = 0;
	GLuint VBO2 = 0;
	GLuint colorVBO = 0;
	GLuint colorVBO2 = 0;
	GLuint anglesVBO = 0;
	GLuint anglesVBO2 = 0;

	float * points;
	float * points2;
	float * colors;
	float * colors2;
	float * angles;
	float * angles2;

	float angle = 0.0f;
	float angleMod = 0.0f;
	float radius = 0.4f;

	float magnitude= 0.4f;

	std::default_random_engine randGenerator;
};

