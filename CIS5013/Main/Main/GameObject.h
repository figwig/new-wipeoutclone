#pragma once
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/type_ptr.hpp>
#include <glew/glew.h>
#include "Config.h"
#define _USE_MATH_DEFINES
#include <math.h>
class Volume;

class GameObject
{
public:
	GameObject() {}
	~GameObject() {}

	virtual void update(double deltaTime) = 0;
	virtual void draw(glm::mat4 camera) = 0;

	//return the position
	glm::vec3 getPos()
	{return glm::vec3(posX, posY, posZ);}

	//return the rotation
	glm::vec3 getRot()
	{return glm::vec3(thetaX, thetaY, thetaZ);}

	glm::mat4 getRotMat()
	{
		return glm::toMat4(
			glm::quat(glm::vec3(0.0f, thetaY, 0.0f))*
			glm::quat(glm::vec3(thetaX, 0.0f, 0.0f))*
			glm::quat(glm::vec3(0.0f, 0.0f, thetaZ)));
	}
	glm::mat4 getTransMat()
	{
		return glm::translate(glm::mat4(1), glm::vec3(posX, posY, posZ));
	}
	void setPos(glm::vec3 pos)
	{
		posX = pos.x;
		posY = pos.y;
		posZ = pos.z;
	}
	void setRot(glm::vec3 pos)
	{
		thetaX = pos.x;
		thetaY = pos.y;
		thetaZ = pos.z;
	}
	void setScale(glm::vec3 pos)
	{
		scaleX = pos.x;
		scaleY = pos.y;
		scaleZ = pos.z;
	}

	void setConfig(Config newConfig) { thisConfig = newConfig; defaultShaders = thisConfig.mainShaders;}

	virtual Volume * getVolume() = 0;
	void setParent(GameObject* newParent) { parent = newParent;  }

protected:

#pragma region Transform modifiers
	float posX = 0.0f;
	float posY = 0.0f;
	float posZ = 0.0f;

	float thetaX = 2*M_PI;
	float thetaY = 2*M_PI;
	float thetaZ = 0.0f;

	float scaleX =1.0f;
	float scaleY = 1.0f;
	float scaleZ = 1.0f;
#pragma endregion

	Config thisConfig;

	GameObject * parent = nullptr;

	GLuint defaultShaders = 0;

};

