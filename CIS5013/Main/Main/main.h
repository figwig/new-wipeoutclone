#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "Game.h"
#define TINYOBJLOADER_IMPLEMENTATION // define this in only *one* .cc
#include "texture_loader.h"
//the quit bool
bool quit = false;


//The Game object
Game* thisGame;

//The init Method to start glew
void init(int argc, char* argv[]);

//My render 
void makeFrame(void);

//Function commands
void mouseDown(int buttonID, int state, int x, int y);
void mouseMove(int x, int y);
void keyEvent(unsigned char Key, int x, int y);
void keyUpEvent(unsigned char Key, int x, int y);
void passiveMouseMove(int, int);

void shaderInit();

Config thisConfig;

GLuint getLightUniformLocationName(int count, int index);


const static int PARAMS_PER_LIGHT = 7;

char * propertyNames[PARAMS_PER_LIGHT] =
{
"position",
"colors",
"attenuation",
"ambientCoefficient",
"coneAngle",
"coneDirection"
,"finalLightMatrix"
};