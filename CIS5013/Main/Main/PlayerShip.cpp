#include "PlayerShip.h"
#include "VertexData.h"
#include "Game.h"
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include <glm-0.9.9.2/glm/glm.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include <vector>
#include "tiny_obj_loader.h"
#include "keyState.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include "Effect3D.h"
#include "HoverJet.h"
#include <glm-0.9.9.2/glm/gtc/quaternion.hpp>

using namespace tinyobj;

PlayerShip::PlayerShip(Game* gameRefr, VAOData * newData)
{
	parent = nullptr;

	dataVAO = newData;

	refr = gameRefr;
	
	scaleX = 0.5f;
	scaleY = 0.5f;
	scaleZ = 0.5f;
}

void PlayerShip::initParts()
{
	ObjExaminer * examiner = new ObjExaminer();
	
	examiner->setFile("Objects/Ship2.obj");
	GLuint engineCone = fiLoadTexture("Textures/EngineCone.jpg");
	GLuint canopyTex = fiLoadTexture("Textures/Canopy.png");
	GLuint spec = fiLoadTexture("Textures/shipSpec.png");

	//leftengine cone
	PlayerObject* leftEngine = new PlayerObject();
	examiner->setCurrShape("ControlFlapsL");
	leftEngine->setVAOData(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), engineCone, spec, 0));
	leftEngine->setDefaultPos(-examiner->getCentre() + glm::vec3(-3.5f, 2.44f, 21.8f));//
	leftEngine->setParent((GameObject*)this);
	
	refr->addGameObject(leftEngine);
	shipParts.leftEngineCone = leftEngine;

	//right engine cone
	PlayerObject* rightEngine = new PlayerObject();
	examiner->setCurrShape("ControlFlapsR");
	rightEngine->setVAOData(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), engineCone, spec, 0));

	rightEngine->setDefaultPos(-examiner->getCentre() + glm::vec3(3.5f, 2.44f, 21.8f));
	rightEngine->setParent(this);


	refr->addGameObject(rightEngine);
	shipParts.rightEngineCone = rightEngine;


	PlayerObject * canopy = new PlayerObject();
	examiner->setCurrShape("Canopy");
	canopy->setVAOData(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), canopyTex, spec, 0));
	canopy->setParent(this);


	refr->addTransparentGameObject(canopy);
	shipParts.canopy = canopy;

	//left engine flames
	GameObject* effectL = new EngineBlue();
	effectL->setParent(this);
	refr->addTransparentGameObject(effectL);
	effectL->setPos(glm::vec3(-1.75f, 1.22f, 10.5f));
	shipParts.leftEngineEffect = (Effect3D*)effectL;

	//right engine flames
	GameObject* effectR = new EngineBlue();
	effectR->setParent(this);
	refr->addTransparentGameObject(effectR);
	effectR->setPos(glm::vec3(1.75f, 1.22f, 10.5f));
	shipParts.rightEngineEffect = (Effect3D*)effectR;

	//left engine flames
	EngineBlue* effectiL = new EngineBlue();
	effectiL->setParent(this);
	effectiL->setRadius(0.18f);
	refr->addTransparentGameObject(effectiL);
	effectiL->setPos(glm::vec3(-1.75f, 1.22f, 10.5f));
	shipParts.insideleftEngineEffect = (Effect3D*)effectiL;

	//right engine flames
	EngineBlue* effectiR = new EngineBlue();
	effectiR->setParent(this);
	effectiR->setRadius(0.18f);
	refr->addTransparentGameObject(effectiR);
	effectiR->setPos(glm::vec3(1.75f, 1.22f, 10.5f));
	shipParts.insiderightEngineEffect = (Effect3D*)effectiR;


	GLuint hoverTex = fiLoadTexture("Textures/hoverJet.jpg");


	char  name[20];
	//Get the hoverjets
	for (int i = 0; i < 10; i++)
	{
		//Run through each hover

		sprintf(name, "Hover%d", i+1);
		examiner->setCurrShape(name);

		shipParts.hoverJets[i] = new PlayerObject();
		shipParts.hoverJetEffects[i] = new HoverJet();

		//Add the hoverjets to the game model
		shipParts.hoverJets[i]->setVAOData(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), hoverTex, spec, 0));
		shipParts.hoverJets[i]->setParent(this);

		//Sort the hoverjetEffects
		shipParts.hoverJetEffects[i]->setParent(this);
		shipParts.hoverJetEffects[i]->setRadius(0.15f);
		shipParts.hoverJetEffects[i]->setPos(examiner->getCentre()/2.0f);


		refr->addGameObject(shipParts.hoverJets[i]);
		refr->addTransparentGameObject(shipParts.hoverJetEffects[i]);

	}

	shipParts.headLight = new Light();

	shipParts.headLight->position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	shipParts.headLight->intensities = glm::vec3(6.5f, 6.0f, 6.0f);
	shipParts.headLight->attenuation = 0.005f;
	shipParts.headLight->ambientCoefficient = 0.0f;
	shipParts.headLight->coneAngle = 15;
	shipParts.headLight->coneDirection = glm::vec3(0.0f, 0.0f, -1.0f);

	//Correcting the headlightPosition
	shipParts.headLight->position = headLightPos;

	thisConfig.currLighting->addLight(shipParts.headLight);
}

PlayerShip::~PlayerShip()
{
	glm::quat rotation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
	
}

void PlayerShip::update(double deltaTime)
{
	Object3D::update(deltaTime);

	//check for input
	checkInput();

	cornerRotate();

	//increment the movement floats
	move();



	//Call Object3D's update, which handles transformations of matrixes based on the movement floats
	Object3D::lateUpdate(deltaTime);

	updateEffects();
}

void PlayerShip::checkInput()
{
	//Get the key state
	Keystate currKeys = refr->getKeys();

	bool acclerativeInput = false;
	bool acclerativeRotInputY = false;
	bool acclerativeRotInputX = false;
	//std::cout << "W";

	if (currKeys & Keys::Up)
	{

		//move forward
		applyAcceleration(glm::vec3(0.0f, 0.0f, -acceleration*deltaTime));
		acclerativeInput = true;
	}


	if (currKeys & Keys::Down)
	{
		//move backward
		applyAcceleration(glm::vec3(0.0f, 0.0f, acceleration*deltaTime*1.05));
		acclerativeInput = true;
	}

	if (currKeys & Keys::Left)
	{
		//turn left
		applyRotation(glm::vec3(0.0f,rotAcceleration*deltaTime, 0.0f));
		intensityR += turningEngineIntensityBase * deltaTime;
		acclerativeRotInputY = true;
	}

	if (currKeys & Keys::Right)
	{
		//turn left
		applyRotation(glm::vec3(0.0f, -rotAcceleration*deltaTime, 0.0f));

		intensityL += turningEngineIntensityBase * deltaTime;

		acclerativeRotInputY = true;
	}

	if (currKeys & Keys::R)
	{
		//tiltup
		applyRotation(  glm::vec3(-rotAcceleration* deltaTime, 0.0f, 0.0f));
		acclerativeRotInputX = true;
	}
	if (currKeys & Keys::F)
	{
		//tiltdown
		applyRotation(glm::vec3(rotAcceleration* deltaTime, 0.0f, 0.0f));
		acclerativeRotInputX = true;
	}
	if (!acclerativeInput)
	{
		if (movement.z > 0)
		{
			movement.z -= acceleration * deltaTime;
		}
		if (movement.z < 0)
		{
			movement.z += acceleration * deltaTime;
		}
	}

	if (!acclerativeRotInputY)
	{
		decayMomentumRot();

	}

	if (!acclerativeRotInputX)
	{
		tendToFlat();
	}
}											  

void PlayerShip::updateEffects()
{
	//Update the headlight's position and matrix
	shipParts.headLight->coneDirection = glm::rotateY(shipParts.headLight->coneDirection, rotation.y);

	shipParts.headLight->finalMatrix = T*R;

	
	//Calculate intensity of the rear engines
	speed = sqrt(sqr(movement.x) + sqr(movement.y) + sqr(movement.z));

	float intensity = baseEngineIntensity;
	float factor = speed/maxSpeed;

	intensity *= factor + multiplictiveFactorAddition;
	

	if (shipParts.leftEngineEffect != nullptr)
	{
		intensityL *= turningEngineIntensityDecay;
		shipParts.leftEngineEffect->setIntensity(0.4f*(intensityL+intensity));
		shipParts.insideleftEngineEffect->setIntensity(insideEngineConeReduction*0.4f*(intensityL+intensity));
	}

	if (shipParts.rightEngineEffect != nullptr)
	{
		intensityR *= turningEngineIntensityDecay;
		shipParts.rightEngineEffect->setIntensity(0.4f*(intensityR + intensity));
		shipParts.insiderightEngineEffect->setIntensity(insideEngineConeReduction*0.4f*(intensityR + intensity));
	}

	//Turn off the engine if the calculated intensity is below a threshold
	if (intensity < minimumEngineEffectThreshold)
	{
		shipParts.rightEngineEffect->setActive(false);
		shipParts.leftEngineEffect->setActive(false);
		shipParts.insiderightEngineEffect->setActive(false);
		shipParts.insideleftEngineEffect->setActive(false);
	}
	else
	{
		shipParts.rightEngineEffect->setActive(true);
		shipParts.leftEngineEffect->setActive(true);
		shipParts.insiderightEngineEffect->setActive(true);
		shipParts.insideleftEngineEffect->setActive(true);
	}

	shipParts.leftEngineCone->setOffset(glm::vec3(0.0f, 0.0f, 0.8f*-(intensity+(intensityL*engineConeMovementMultiplier))));
	shipParts.rightEngineCone->setOffset(glm::vec3(0.0f, 0.0f, 0.8f*-(intensity+(intensityR*engineConeMovementMultiplier))));
}

void PlayerShip::draw(glm::mat4 camera)
{
	defaultShaders = thisConfig.mainShaders;
	Object3D::draw(camera);
}

//This is a recursive method that tests if existing momemntum plus speed will go over max speed, and if it does it calls itself with a third of the initial acceleration. If the current 
//if the current speed plus the acceleration doesn't go over max, then apply that speed

void PlayerShip::applyAcceleration(glm::vec3 vector)
{
	if (sqrt(sqr(vector.x) + sqr(vector.y) + sqr(vector.z)) < 0.00005f)
	{
		return;
	}

	float currSpeed = sqrt( sqr(movement.x+ vector.x) + sqr(movement.y + vector.y) + sqr(movement.z+ vector.z));


	if (movement.z < maxSpeed)
	{
		movement += vector;
	}
	else
	{
		std::cout << "speed too fast\n";
		movement.z = -maxSpeed/deltaTime;
	}
	speed = currSpeed;
}

void PlayerShip::applyRotation(glm::vec3 vector)
{
	//Is the operation large enough to bother interating for?
	if (sqrt(sqr(vector.x) + sqr(vector.y) + sqr(vector.z)) < 0.0001f)
	{
		return;
	}

	//Work out current magnitude of movement

	float currRotSpeed = sqrt(sqr(rotation.x + vector.x) + sqr(rotation.y + vector.y) + sqr(rotation.z + vector.z));

	// if the speed is lower than the rotspeed * deltaTime
	if (currRotSpeed* deltaTime < maxRotSpeed)
	{
		//apply acceleration
		rotation += vector;
	}
	else
	{
		applyRotation(glm::vec3(vector.x *0.7, vector.y * 0.7, vector.z * 0.7));
	}
	rotSpeed = currRotSpeed;
}

void PlayerShip::applyVertAcceleration(glm::vec3 vector)
{
	if (sqrt(sqr(vector.x) + sqr(vector.y) + sqr(vector.z)) < 0.00005f)
	{
		return;
	}

	float speed = sqrt(sqr(movement.x + vector.x) + sqr(movement.y + vector.y) + sqr(movement.z + vector.z));


	if (speed* deltaTime < maxVertSpeed)
	{
		movement += vector;
	}
	else
	{
		std::cout << speed << "speed too fast\n";
		applyAcceleration(glm::vec3(vector.x*0.2, vector.y*0.2, vector.z*0.2));
	}
}

void PlayerShip::move()
{
	//posX += movement.x;
	posZ += movement.z *cos(thetaY);
	posX += movement.z *sin(thetaY);

	//Up and Down Movement
	posY += movement.z * sin(-thetaX);

	thetaX += rotation.x;
	thetaY += rotation.y;
	thetaZ += rotation.z;
}

void PlayerShip::decayMomentum()
{
	speed = sqrt(sqr(movement.x) + sqr(movement.y) + sqr(movement.z));

	glm::vec3 normalized = glm::normalize(movement);

	if (speed > maxSpeed*0.2)
	{
		movement -= normalized* (glm::vec3(fixedDecay, fixedDecay, fixedDecay) * glm::vec3(deltaTime,deltaTime,deltaTime));
	}
	else
	{
		movement += drag * (-movement);
	}


}

void PlayerShip::decayMomentumRot()
{
	rotation *= rotDrag* rotDrag;
}

void PlayerShip::cornerRotate()
{
	if ( thetaZ>= maxTilt)
	{
		thetaZ *= rotDrag * rotDrag*rotDrag;
	}
	else if (thetaZ < -maxTilt)
	{
		thetaZ *= rotDrag * rotDrag*rotDrag;
	}
	else if (rotation.y*deltaTime < -rotTiltThreshold)
	{
		thetaZ -= 0.002f ;

	}
	else if (rotation.y*deltaTime > rotTiltThreshold)
	{
		thetaZ += 0.002f;
	}
	else if (rotation.y*deltaTime > rotTiltThreshold / 2 || rotation.y < -rotTiltThreshold / 2)
	{
		thetaZ *= rotDrag*rotDrag*rotDrag;
	}
	else
	{
		thetaZ *= rotDrag;
	}
}

void PlayerShip::tendToFlat()
{
	thetaX *= pow(xRotDecay, deltaTime*xDecayReductionPerFrame);
}
