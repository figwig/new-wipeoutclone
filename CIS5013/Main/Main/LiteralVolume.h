#pragma once
#include "Volume.h"

//RectVol literally uses a check point for each vertex from each model. Its check method assumes the object is convex. use for testing only
class ConvexVol :
	public Volume
{
public:
	ConvexVol();
	~ConvexVol();

	bool checkPoint(glm::vec3);
	void preCheckCalcs(Volume *) {}
	void createVolume(glm::vec3 centre);
	bool checkVolume(Volume *);
};

