#include "Game.h"
#include <FreeImage\FreeImagePlus.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "Game.h"
#include "Scene.h"
#include "LevelOne.h"
#include "PlayerShip.h"
#include "Scenery.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtx/rotate_vector.hpp>
#include "LoadScreen.h"
#include <oal_ver1.1/include/alc.h>
#include <oal_ver1.1/include/al.h>
#include "GURiffModel.h"
#include <mmreg.h>
#include "NightScene.h"

using namespace std;
using namespace CoreStructures;


Game::Game(Config newConfig)
{
	thisConfig = newConfig;

	gameClock = new Clock();
	gameClock->tick();


	
#pragma region Sound Stuff

	currAudioDevice = alcOpenDevice(NULL);
	currContext = alcCreateContext(currAudioDevice, NULL);
	
	alcMakeContextCurrent(currContext);
	alGenBuffers(1, &sound1);

	auto soundData = new GURiffModel("Sounds/jetEngine.wav");

	WAVEFORMATEXTENSIBLE wv;
	
	memcpy_s(&wv, sizeof(WAVEFORMATEXTENSIBLE), soundData->riffChunkForKey(' tmf').data, soundData->riffChunkForKey(' tmf').chunkSize);

	alBufferData(
		sound1,
		AL_FORMAT_MONO16,
		(ALvoid*)soundData->riffChunkForKey('atad').data,
		(ALsizei)soundData->riffChunkForKey('atad').chunkSize,
		(ALsizei)wv.Format.nSamplesPerSec
	);

	alGenSources(1 ,&source1);

	alSourcei(source1, AL_BUFFER, sound1);

	ALfloat listenerVel[] = { 0.0f, 0.0f, 0.0f };

	ALfloat listenerOri[] = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };



	alListener3f(AL_POSITION,0,0,0 );

	alListenerfv(AL_VELOCITY, listenerVel);

	alListenerfv(AL_ORIENTATION, listenerOri);
#pragma endregion
}

Game::~Game()
{
}

void Game::Start()
{
	//Make some scenes
	scenes[0] = new LoadScreen();

	scenes[1] = new LevelOne();

	scenes[2] = new NightScene();

	currScene = 0;

	scenes[0]->load(this);

	scenes[0]->run();

	if (gameCamera != nullptr)
	{
		gameCamera->update(deltaTime, 0, 0, false);
	}
	
}

void Game::render(void)
{
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_BLEND);

	GameObject * currObject;

	//Render all the renderable objects in the active scene
	int max = scenes[currScene]->getNumObjects();

	for (int count = 0; count < max; count++)
	{
		currObject = scenes[currScene]->getObjectByIndex(count);
		if (currObject != nullptr)
		{

			currObject->draw(cameraMatrix);
		}
	}

	//Render all the transparant objects!
	max = scenes[currScene]->getNumTransObjects();
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (int count = 0; count < max; count++)
	{
		currObject = scenes[currScene]->getTransObjectByIndex(count);
		if (currObject != nullptr)
		{

			currObject->draw(cameraMatrix);
		}

	}

}

void Game::update(void)
{
	//Tick the clock to count time etc
	gameClock->tick();

	//Fill in the local value for the deltaTime
	deltaTime = gameClock->getDelta();


	if (gameCamera != nullptr)
	{	//Update the camera with the mouse position, play bool
		gameCamera->update(deltaTime, diffX, diffY, playing);
		//Get a local copy of the camera matrix
		cameraMatrix = gameCamera->getCameraMat();

		//tell current lighting the cameraPosition
		thisConfig.currLighting->cameraPos = gameCamera->getCameraPos();

		thisConfig.currLighting->cameraWithOutPersp = gameCamera->getCameraMatNoPersp();

		thisConfig.currLighting->persp = gameCamera->getPersp();
	}

	//Update everything in the renderables list

	//Container for the currentObject
	GameObject * currObject;
	
	//Get the total number of objects
	int max = scenes[currScene]->getNumObjects();

	//loop through all the objects
	for (int count = 0; count < max; count++)
	{
		//get the current object
		currObject = scenes[currScene]->getObjectByIndex(count);
		//validation
		if (currObject != nullptr)
		{
			currObject->update(deltaTime);


			//does the object need to perform collision checks?
			Volume * currVol = currObject->getVolume();
			if (currVol != nullptr)
			{
				//give it a scene copy so it can check for potential collisions
				currVol->checkForCollisionsScene(scenes[currScene]);
			}

		}
	}

	//And then the transparent list
	max = scenes[currScene]->getNumTransObjects();
	for (int count =0; count < max; count++)
	{
		currObject = scenes[currScene]->getTransObjectByIndex(count);
		if (currObject != nullptr)
		{
			currObject->update(deltaTime);
		}
	}

	//Update the lightingstate (does nothing yet)
	thisConfig.currLighting->update();

	//Tell glut to  update itself
	glutPostRedisplay();
}

void Game::setConfigAll()
{

	for (int count = 0; count < scenes[currScene]->getNumObjects(); count++)
	{
		if (scenes[currScene]->getObjectByIndex(count) != nullptr)
		{

			scenes[currScene]->getObjectByIndex(count)->setConfig(thisConfig);

		}
	}
	for (int count = 0; count < scenes[currScene]->getNumTransObjects(); count++)
	{
		if (scenes[currScene]->getTransObjectByIndex(count) != nullptr)
		{
			scenes[currScene]->getTransObjectByIndex(count)->setConfig(thisConfig);

		}
	}
}

void Game::addGameObject(GameObject * newObject)
{
	scenes[currScene]->addGameObject(newObject);
}

void Game::addTransparentGameObject(GameObject * newObject)
{
	scenes[currScene]->addTransparentGameObject(newObject);
}

#pragma region Event Handlers

void Game::mouseDown(int buttonID, int state, int x, int y)
{
	if (mouseOnHold) {
		if (buttonID == GLUT_LEFT_BUTTON)
		{
			if (state == GLUT_DOWN)
			{
				mousePrevX = x;
				mousePrevY = y;
				mouseDownBool = true;
			}
			else if (state == GLUT_UP)
			{
				//Reset the diffX and Y variables
				diffX = 0.0f;
				diffY = 0.0f;

				mouseDownBool = false;
			}
		}
		else
		{
			return;
		}
	}
	else 
	{
		diffX = x;
		diffY = y;
	}
}

void Game::mouseMove(int x, int y)
{
	if (mouseOnHold)
	{
		diffX = x - mousePrevX;
		diffY = y - mousePrevY;
	}
	else
	{
		diffX = x;
		diffY = y;
	}
}

void Game::keyEvent(unsigned char Key, int x, int y, bool upDown)
{
	//if this is a keyDown event
	if (upDown)
	{
		switch (Key)
		{
		case 'w':
			keys |= Keys::Up;

			break;
		case 'W':
			keys |= Keys::Up;
			break;
		case 's':
			keys |= Keys::Down;
			break;
		case 'S':
			keys |= Keys::Down;
			break;
		case 'a':
			keys |= Keys::Left;
			break;
		case 'A':
			keys |= Keys::Left;
			break;
		case 'd':
			keys |= Keys::Right;
			break;
		case 'D':
			keys |= Keys::Right;
			break;
		case 'r':
			keys |= Keys::R;
			break;
		case 'R':
			keys |= Keys::R;
			break;
		case 'f':
			keys |= Keys::F;
			break;
		case 'F':
			keys |= Keys::F;
			break;
		case '1':
			keys |= Keys::num1;
			break;
		case '2':
			keys |= Keys::num2;
			break;
		default:

			//

			break;
		}
	}
	//if this is a keyUp event
	else
	{
		switch (Key)
		{
		case 'w':
			keys &= (~Keys::Up);
			break;
		case 's':
			keys &= (~Keys::Down);
			break;
		case 'a':
			keys &= (~Keys::Left);
			break;
		case 'd':
			keys &= (~Keys::Right);
			break;
		case 'r':
			keys &= (~Keys::R);
			break;
		case 'f':
			keys &= (~Keys::F);
			break;
		case 'W':
			keys &= (~Keys::Up);
			break;
		case 'S':
			keys &= (~Keys::Down);
			break;
		case 'A':
			keys &= (~Keys::Left);
			break;
		case 'D':
			keys &= (~Keys::Right);
			break;
		case 'R':
			keys &= (~Keys::R);
			break;
		case 'F':
			keys &= (~Keys::F);
			break;
		case '1':
			keys &= (~Keys::num1);
			break;
		case '2':
			keys &= (~Keys::num2);
			break;
		default:
			//

			break;
		}
	}
}

void Game::mousePassiveMouse(int x, int y )
{
	if (captureMouse)
	{
		static int centerX = glutGet(GLUT_WINDOW_WIDTH) / 2;
		static int centerY = glutGet(GLUT_WINDOW_HEIGHT) / 2;



		glutPostRedisplay();

		glutWarpPointer(centerX, centerY);
	}
	if (!mouseOnHold)
	{
		diffX = x;
		diffY = y;
	}
}

Keystate Game::getKeys()
{
	return keys;
}

#pragma endregion

