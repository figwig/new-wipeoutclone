#pragma once
#include "Scene.h"
class NightScene :
	public Scene
{
public:
	NightScene();
	~NightScene();

	void run();
	void loadTextures();
	void load(Game* newGame);

private:

	GameObject * player;

	GLuint asphalt = 0;
	GLuint asphaltSpec = 0;
	GLuint asphaltNormal = 0;
	GLuint metal = 0;

	GLuint brickWall = 0;
	GLuint brickWallNormal = 0;

	GLuint shipSpec = 0;
};

