#include "Skybox.h"
#include "texture_loader.h"
#include <CoreStructures\CoreStructures.h>
#include <stdio.h>


Skybox::Skybox(int boxNo)
{
	applyScaling();
	loadTextures(boxNo);
	sortVAO();
}


Skybox::~Skybox()
{
}

void Skybox::update(double deltaTime)
{
	Object3D::update(deltaTime);

	Object3D::lateUpdate(deltaTime);

}

void Skybox::applyScaling()
{
	for (int i = 0; i< 36*3;i++)
	{
		skyboxVertices[i] *= scale;
	}
}

void Skybox::draw(glm::mat4 camera)
{
	glDepthMask(GL_FALSE);
	glUseProgram(thisConfig.skyboxShaders);
	// ... set view and projection matrix

	glUniformMatrix4fv(thisConfig.locCs, 1, GL_FALSE, (GLfloat*) &camera);
	glUniformMatrix4fv(thisConfig.locPs, 1, GL_FALSE, (GLfloat*) &CoreStructures::GUMatrix4::infinitePerspectiveProjection);

	glBindVertexArray(thisVAO);

	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glBindVertexArray(0);

	glDepthMask(GL_TRUE);
}

void Skybox::sortVAO()
{
	glGenVertexArrays(1, &thisVAO);
	glBindVertexArray(thisVAO);
	
	//Setup the Vertex buffer
	glGenBuffers(1, &vertsVBO);
	glBindBuffer(GL_ARRAY_BUFFER, vertsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*3*36, skyboxVertices, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindVertexArray(0);
}

void Skybox::loadTextures(int boxNo)
{
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);


	if (boxNo == 0)
	{
		for (unsigned int i = 0; i < facesPaths.size(); i++)
		{
			fiLoadTextureSkybox(facesPaths[i].c_str(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
		}
	}
	else if (boxNo == 1)
	{
		for (unsigned int i = 0; i < facesPaths2.size(); i++)
		{
			fiLoadTextureSkybox(facesPaths2[i].c_str(), GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
		}
	}


	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

}
