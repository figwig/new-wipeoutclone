#include "EngineBlue.h"
#include <CoreStructures/CoreStructures.h>
#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <random>

///<Summary>
///This is an effect that is always a child of a parent, and the draw call  reflects that. It generates a circle and a blue ring around it.
///it use effect shaders number 1 
///</Summary>


EngineBlue::EngineBlue()
{
	generateCircle(40,radius);
	sortVAO();
}

EngineBlue::~EngineBlue()
{
}

void EngineBlue::draw(glm::mat4 camera)
{
	if (active) {
		
		generateCircle(50, radius);
		sortVAO();
		angleMod += 0.01;

		angle = angleMod;
		
		if (angle > (M_PI-0.01f))
		{
			angle = 0.0f;
			angleMod = 0.0f;
		}

		timePassed += deltaTime;
		glUseProgram(thisConfig.effectShaders1);

		//Tell the shaders all the relevant information this frame
		shaderUniformSetup(camera);

		glBindVertexArray(VAO);

		glDrawArrays(GL_POINTS, 0, totalPoints);
	}
}

void EngineBlue::generateCircle(int noPoints, float radius)// add z co-ord variation!
{
	std::uniform_int_distribution<int> distribution(800, 999);
	std::uniform_int_distribution<int> distribution2(180,600);

	float interval = (M_PI * 2) / noPoints;

	points = new float[noPoints*3];
	colors = new float[noPoints*4];
	angles = new float[noPoints];
	
	int count = 0;
	int countColor = 0;
	int angleCount = 0;

	for (angle; angle < (M_PI * 2)+ angleMod; angle += interval)
	{
		float blueValue = distribution(randGenerator) / 1000.0f;
		float rgValue = distribution2(randGenerator) / 1000.0f;

		points[count] = radius * cos(angle);
		colors[countColor] = rgValue;
		count++;
		countColor++;

		points[count] = radius * sin(angle);
		colors[countColor] = rgValue;
		count++;
		countColor++;

		points[count] = 0.0f;

		
		colors[countColor] =blueValue;
		count++;
		countColor++;

		colors[countColor] = 0.04f;
		countColor++;

		angles[angleCount] = angle;
		angleCount++;
	}

	totalPoints = noPoints;
}

void EngineBlue::shaderUniformSetup(glm::mat4 camera)
{
	//Tell all the shaders various stuff
	//Transform Matrix
	glUniformMatrix4fv(thisConfig.locTe1, 1, GL_FALSE, (GLfloat*)&T);
	//Rotation Matrix
	glUniformMatrix4fv(thisConfig.locRe1, 1, GL_FALSE, (GLfloat*)&R);
	//Scale matrix
	glUniformMatrix4fv(thisConfig.locSe1, 1, GL_FALSE, (GLfloat*)&S);
	//Camera matrix
	glUniformMatrix4fv(thisConfig.locCe1, 1, GL_FALSE, (GLfloat*)&camera);


	//The magnitude
	glUniform1f(thisConfig.e1LocMag,magnitude);

	//Target Vector
	glUniform4f(thisConfig.e1LocTarget, target.x, target.y, target.z, 0.0f);

	//And the position relative to the parent 
	glUniform4f(thisConfig.e1LocPosRel, posX, posY, posZ, 0.0f);

	//And finally the time
	glUniform1f(thisConfig.e1LocTime, (float)timePassed);

}

void EngineBlue::sortVAO()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints * 3, points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &colorVBO);
	glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints * 4, colors, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &anglesVBO);
	glBindBuffer(GL_ARRAY_BUFFER, anglesVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*totalPoints, angles, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	delete[] points;
	delete[] colors;
	delete[] angles;
}
