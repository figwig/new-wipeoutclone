#pragma once
#include <FreeImage\FreeImagePlus.h>
#include <wincodec.h>
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <iostream>
#include "Game.h"
#include "Point3f.h"
#include "Point2f.h"

class VertexData
{

private:
	int vertCount = 0;

	static const int MAX_VERTEXES = 8192;
	
	Point3f* vertexes[MAX_VERTEXES] = { nullptr };
	
	Point3f* normals[MAX_VERTEXES] = { nullptr };

	Point2f* texts[MAX_VERTEXES] = { nullptr };

public:

	VertexData() {}
	~VertexData() {}


	//Accessor Methods
	Point3f** getVertexes() { return vertexes; }
	Point3f** getNormals() { return normals; }
	Point2f** getTexts() { return texts; }

	float* getVertsf();
	bool vertsGot = false;
	float points[MAX_VERTEXES * 3] = { 0.0f };


	float* getNormsf();
	bool normsGot = false;
	float normalsf[MAX_VERTEXES * 3] = { 0.0f };

	float* getTexts2f();
	bool textsGot = false;
	float textsf[MAX_VERTEXES * 2] = { 0.0f };

	int getVertCount() { return vertCount; }

	void setVertCount(int vc) { vertCount = vc; }

	void addVertex(Point3f* newVertex, int index);
	void addNormal(Point3f* newNormal, int index);

	void clearVertexes() {  }

	void removeVertex(Point3f vertex);
	void removeVertex(int index);

	void addVertexes(Point3f * newVertexes[], int size);
	void addNormals(Point3f * newNormals[], int size);
	void addTextUvs(Point2f * newTexts[], int size);

	
};

