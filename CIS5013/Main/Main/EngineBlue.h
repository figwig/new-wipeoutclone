#pragma once
#include "Effect3D.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#include <random>
class EngineBlue :
	public Effect3D
{
public:
	EngineBlue();
	~EngineBlue();

	void draw(glm::mat4);
	void setRadius(float x) { radius = x; }

protected:
	 
	void generateCircle(int noPoints, float radius);
	void shaderUniformSetup(glm::mat4 camera);
	void sortVAO();

	GLuint VBO = 0;
	GLuint colorVBO = 0;
	GLuint anglesVBO = 0;

	float * points;
	float * colors;
	float * angles;

	float angle = 0.0f;
	float angleMod = 0.0f;
	float radius = 0.4f;

	std::default_random_engine randGenerator;

};

