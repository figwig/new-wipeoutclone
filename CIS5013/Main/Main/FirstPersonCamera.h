#pragma once
#include "Camera.h"
class FirstPersonCamera :
	public Camera
{
public:
	FirstPersonCamera(GameObject * player);
	~FirstPersonCamera();

	void update(double deltaTimed, int mouseX, int mouseY, bool playing);

protected:
	GameObject * player;
	float y = 0.0f;

	glm::vec2 cameraAngle = glm::vec2(0.0f, 0.0f);

	glm::vec3 lookingAtConst = glm::vec3(0.0f, 0, -5.0f);
	glm::vec3 targetAdjust = glm::vec3(0, 3, 0);

	glm::vec4 positionOffset = glm::vec4(0, -3, 1, 0);
	
	float mouseSens = 500.0f;
	float maxX = 1.0f;

};

