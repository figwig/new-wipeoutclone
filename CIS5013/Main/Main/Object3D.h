#pragma once
#include "GameObject.h"
#include "VertexData.h"
#include <glm-0.9.9.2/glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm-0.9.9.2/glm/gtc/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtc/matrix_transform.hpp>
#include <glm-0.9.9.2/glm/gtx/quaternion.hpp>
#include <glm-0.9.9.2/glm/gtx/transform.hpp>
#include <stdio.h>
#include <vector>
#include "tiny_obj_loader.h"
#include <glew\glew.h>
#include <freeglut\freeglut.h>
#include <CoreStructures\CoreStructures.h>
#include <FreeImage\FreeImagePlus.h>
#include "texture_loader.h"
#include "VAOData.h"
#include "Volume.h"



class Object3D : public GameObject
{
public:
	Object3D() {}
	~Object3D() {}
	void setVAOData(VAOData* newData) { dataVAO = newData; }
	void draw(glm::mat4 camera);

	void update(double deltaTime);
	void lateUpdate(double deltaTime);
	void setShinyness(int newShiny) { shinyness = newShiny; }
    Volume * getVolume() { 
		if (dataVAO != nullptr) {
			return dataVAO->getVolume();
		}
		else
		{
			return nullptr;
		}
	}

	bool drawTangent = false;
	bool drawNormal = false;

	void setVolumeScale(glm::vec3 scale)
	{
		if (dataVAO != nullptr) {
			if (dataVAO->getVolume() != nullptr)
			{
				dataVAO->getVolume()->setScale(scale);
			}
		}
	}
	 
protected:

	double deltaTime;
	
	VAOData * dataVAO = nullptr;

	//Transform Matricie

	glm::mat4 R = glm::toMat4( glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));
	glm::mat4 pR =glm::toMat4(glm::quat(glm::vec3(0.0f, 0.0f, 0.0f)));

	glm::mat4 T = glm::mat4();
	glm::mat4 pT = glm::translate(glm::mat4(),glm::vec3(0,0,0));

	CoreStructures::GUMatrix4 S=  CoreStructures::GUMatrix4::scaleMatrix(1.0f, 1.0f, 1.0f);

	float shinyness = 50.0f;


	void setLightingUniforms(glm::mat4 camera);
	
	void drawNormals(glm::mat4 camera);
	void drawTangents(glm::mat4 camera);
	
};

