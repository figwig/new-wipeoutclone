#include "Volume.h"
#include "Scene.h"

Volume::Volume()
{
}


Volume::~Volume()
{
}

void Volume::checkForCollisionsScene(Scene * scene)
{
	int totalObjects = scene->getNumObjects();
	for (int count = 0; count < totalObjects; count++)
	{
		GameObject* currObject = scene->getObjectByIndex(count);


		if (currObject->getVolume() != nullptr)
		{	
			if (currObject->getVolume() != this) {

				//This object has a volume also, and it isn't this object!

				//Check if the object isn't too far to be relevant
				preCheckCalcs(currObject->getVolume());
				
				//get a transformation matrix for the visiting volume
				glm::mat4 currMat = currObject->getTransMat() *currObject->getRotMat();


				checkVolume(currObject->getVolume());
			}
		}

	}
}
