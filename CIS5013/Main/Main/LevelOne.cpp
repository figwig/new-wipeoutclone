#include "LevelOne.h"
#include "PlayerShip.h"
#include "WorldObject.h"
#include "Scenery.h"
#include <string>
#include "ObjExaminer.h"
#include <stdio.h>
#include "Effect3D.h"
#include "EngineBlue.h"
#include "Skybox.h"
#include "PlayerObject.h"
#include "smallSpotLight.h"
#include "ThirdPersonCamera.h"

LevelOne::LevelOne()
{
	name = "WipeOut";
}

LevelOne::~LevelOne()
{
	delete currLighting;
}
void LevelOne::loadTextures()
{
	metal = fiLoadTexture("Textures/metal.jpg");
	redMetal = fiLoadTexture("Textures/redMetal.jpg");
	asphalt = fiLoadTexture("Textures/Asphalt.png");
	asphaltSpec = fiLoadTexture("Textures/AsphaltSpec.png");
	asphaltNormal = fiLoadTexture("Textures/AsphaltNormal.png");
	road = fiLoadTexture("Textures/road.jpg");
	shipSpec = fiLoadTexture("Textures/shipSpec.png");
	concrete = fiLoadTexture("Textures/Concrete.jpg");
	billBoard = fiLoadTexture("Textures/BillBoard.png");
	shipTex = fiLoadTexture("Textures/Ship.png");
}
void LevelOne::load(Game* thisGame)
{	///<Summary>
	///load is the init call and loads in all the large assets and textures for the game 
	///Each shape that needs a specific texture is pulled seperately from the obj file
	///meta geometry is used to place objects that are repeated.
	///</Summary>
	gameRefr = thisGame;

	ThirdPersonCamera * camera = new ThirdPersonCamera();


	gameRefr->setCamera(camera);

	currLighting = new LightingState();

	gameRefr->thisConfig.currLighting = currLighting;

	//Save the reference for later
	
	examiner = new ObjExaminer();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);


	loadTextures();

	loadWorld();
	loadPlayer();

	camera->setTarget(player);

	//load  in any non-world objects
	examiner->setFile("Objects/FloatCity.obj");

	examiner->setCurrShape("City");



	Custom *floatingCity = new Custom(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), metal, 0,0),
		
		glm::vec3(10, 600, 0),
		glm::vec3(10, 10, 10),
		glm::vec3(0, 0, 0));

	addGameObject(floatingCity);

	Light *mainLight = new Light();

	mainLight->position = glm::vec4(-0.9, 0.9, -0.9, 0.0);
	mainLight->intensities = glm::vec3(0.5, 0.5, 0.5);
	mainLight->attenuation = 0.12;
	mainLight->ambientCoefficient = 0.12;
	mainLight->coneAngle = 0;
	mainLight->coneDirection = glm::vec3(0, 0, 0);

	currLighting->addLight(mainLight);

	Light *bottomLight = new Light();

	bottomLight->position = glm::vec4(-0.2, -1.0, 0.0, 0.0);
	bottomLight->intensities = glm::vec3(0.1, 0.1, 0.1);
	bottomLight->attenuation = 0.12;
	bottomLight->ambientCoefficient = 0.006;
	bottomLight->coneAngle = 0;
	bottomLight->coneDirection = glm::vec3(0, 1, 0);

	//currLighting->addLight(bottomLight);

	Light *sideLight = new Light();

	sideLight->position = glm::vec4(0.0, 20.0, 0.0, 1.0);
	sideLight->intensities = glm::vec3(1.9, 0.9, 0.9);
	sideLight->attenuation = 0.00001;
	sideLight->ambientCoefficient = 0.01;
	sideLight->coneAngle = 85;
	sideLight->coneDirection = glm::vec3(0.0, -1.0, 0.0);

	//currLighting->addLight(sideLight);


	Light * redSpot = new smallSpotLight(glm::vec3(5.0f, 0.2f, 0.2f), glm::vec3(-4, 4, 0));
	Light * greenSpot = new smallSpotLight(glm::vec3(0.0f, 0.1f, 4.8f), glm::vec3(0, 4, -4));
	Light * blueSpot = new smallSpotLight(glm::vec3(0.1f, 4.9f, 0.2f), glm::vec3(4, 4, 0));


	currLighting->addLight(redSpot);
	currLighting->addLight(blueSpot);
	currLighting->addLight(greenSpot);

	gameRefr->setConfigAll();
}

void LevelOne::run()
{
	//Tell the game who the player is
	gameRefr->setPlayer(player);

	gameRefr->setPlaying(true);
}

void LevelOne::loadWorld()
{
	//Import the World
	examiner->setFile("Objects/World.obj");

	examiner->setCurrShape("Track");

	WorldObject * track = new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), asphalt, asphaltSpec, asphaltNormal));


	addGameObject(track);


	examiner->setCurrShape("BarrierL");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));


	examiner->setCurrShape("BarrierR");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));


	examiner->setCurrShape("Ground");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), concrete, shipSpec, 0)));


	examiner->setCurrShape("Start");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));

	examiner->setCurrShape("Banner");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), billBoard, shipSpec, 0)));

	examiner->setCurrShape("BannerSupport1");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));
	
	examiner->setCurrShape("BannerSupport2");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));

	examiner->setCurrShape("Base");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));

	examiner->setCurrShape("StartGround");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), metal, shipSpec, 0)));

	examiner->setCurrShape("Canopy");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));

	examiner->setCurrShape("Cage");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));

	examiner->setCurrShape("StartSceneryL");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));

	examiner->setCurrShape("StartSceneryR");

	addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));


	for (int i = 0; i < 5; i++)
	{
		char  name[20];
		sprintf(name, "StrutSupport%d", i);

		//Run through each structSupport
		examiner->setCurrShape(name);
		addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));

	}	
	for (int i = 0; i < 5; i++)
	{
		char  name[20];
		sprintf(name, "Building%d", i);

		//Run through each structSupport
		examiner->setCurrShape(name);
		addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));
	}

	for (int i = 0; i < 13; i++)
	{
		char  name[20];
		sprintf(name, "RoadSupport%d", i);

		//Run through each structSupport
		examiner->setCurrShape(name);
		addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), road, shipSpec, 0)));

	}

	for (int i = 0; i < 3; i++)
	{
		
		char  name[20];
		sprintf(name, "Stand%d", i);

		//Run through each structSupport
		examiner->setCurrShape(name);
		addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));

	}
	for (int i = 0; i < 4; i++)
	{
		char  name[20];
		sprintf(name, "Scenery%d", i);

		//Run through each structSupport
		examiner->setCurrShape(name);
		addGameObject(new WorldObject(new VAOData(examiner->getVAO(), examiner->getTotalVerts(), redMetal, shipSpec, 0)));

	}


	//All hard work is done, now just tell game to stop with thr load scene and add the game objects


}

void LevelOne::loadPlayer()
{
	examiner->setFile("Objects/ship2.obj");
	examiner->setCurrShape("Ship");

	ship = examiner->getVAO();

	//Create a player
	player = new PlayerShip(gameRefr, new VAOData(ship, examiner->getTotalVerts(), shipTex, shipSpec, 0));
	player->setConfig(gameRefr->getConfig());
	addGameObject(player);
	player->initParts();
}
