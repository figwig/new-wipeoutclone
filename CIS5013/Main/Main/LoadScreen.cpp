#include "LoadScreen.h"
#include "ImageRect.h"
#include "MenuController.h"

LoadScreen::LoadScreen()
{
}


LoadScreen::~LoadScreen()
{
	delete menuController;
}

void LoadScreen::run()
{
	printf("Runnning Load Screen Scene");

}

void LoadScreen::load(Game * thisGame)
{
	gameRefr = thisGame;

	currLighting = new LightingState();

	gameRefr->thisConfig.currLighting = currLighting;

	loadImg = new ImageRect("Textures/load.jpg");

	loadImg->sortVAO();

	addGameObject(loadImg);

	menuController = new MenuController(gameRefr);

	addGameObject(menuController);

	gameRefr->setConfigAll();
}
