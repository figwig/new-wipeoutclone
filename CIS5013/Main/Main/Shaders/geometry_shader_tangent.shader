#version 330 core
layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

in VS_OUT {

    vec3 tangent;
	vec3 normal;

} gs_in[];

const float MAGNITUDE = 0.8;

void GenerateLineAtVertex(int index)
{
	vec3 testSampledNormal = vec3(0, 0, 1);
	
	vec3 bitangent = cross(gs_in[index].normal, gs_in[index].tangent);
    
	mat3 TBN = mat3(gs_in[index].tangent, bitangent, gs_in[index].normal);

	testSampledNormal = TBN * testSampledNormal;


	gl_Position = gl_in[index].gl_Position;

    EmitVertex();

    gl_Position =
		 gl_in[index].gl_Position 
		+  vec4(testSampledNormal, 0.0) * MAGNITUDE;


    EmitVertex();


    EndPrimitive();
}


void main()
{
    GenerateLineAtVertex(0); // first vertex normal
    GenerateLineAtVertex(1); // second vertex normal
    GenerateLineAtVertex(2); // third vertex normal



	float length0 = length(gl_in[0].gl_Position - gl_in[1].gl_Position);
	float length1 = length(gl_in[0].gl_Position - gl_in[2].gl_Position);
	float length2 = length(gl_in[1].gl_Position - gl_in[2].gl_Position);

}  



