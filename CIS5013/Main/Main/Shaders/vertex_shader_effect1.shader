#version 330 core

uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix
uniform mat4 projection; // the projection matrix


layout (location = 0) in vec3 position;
layout (location = 1) in vec4 aColor;
layout(location = 2) in float angle;

//The full out packet going to the geo shader
out VS_OUT {
    vec4 color;
	vec4 position;
	mat4 finalMatrix;
	float angle;
} vs_out;

void main()
{
	// Saving the pos as a local variable so I can modify it
	vec4 pos = vec4(position.x, position.y, position.z, 1.0);

	//Calculate the total transform matrix
	mat4 transform = T* R * S;

	//For now, set gl_position to just be pos. For this shader, the geometry shader will apply
	//Transformations
	gl_Position = pos;


	//Pass through position, the projection and the direction vector
	vs_out.position = pos;
	vs_out.finalMatrix = camera * transform ;


	//Pass through color and angle
	vs_out.color = aColor;
	vs_out.angle = angle;

}