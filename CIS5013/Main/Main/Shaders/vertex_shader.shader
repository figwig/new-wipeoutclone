#version 330


uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix
uniform vec4 posRelParent; // the position relative to the parent


// Input vertex packet
layout (location = 0) in vec4 position;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec4 tangent;
layout (location = 4) in vec4 bitangent;
layout (location = 8) in vec2 textureCoord;


// Output vertex packet
out packet {

	vec2 textureCoord;
	vec3 normal;
	vec3 vert;
	mat3 TBN;
	vec3 tangent;
	vec3 bitangent;
	vec3 normalTBN;

} outputVertex;

mat4 transform;
mat3 TBN;

void calculateTBN()
{
	//get the model matrix, the transform of the object with scaling and transform  removeds
	mat3 model = mat3(transpose(inverse(transform)));
	
	vec3 T = normalize(model*tangent.xyz);

	vec3 N = normalize(model*normal);
	
	//I used to retrieve the bitangents by crossing the normal and tangent but now they are calculated independently
	vec3 B = normalize(model*bitangent.xyz);

	TBN = mat3( T , B , N);

	outputVertex.TBN = TBN;

	//Pass though TBN vectors for colour debugging in the fragment shader
	outputVertex.tangent = T;
	outputVertex.bitangent = B;
	outputVertex.normalTBN = N;


}

void main(void) {
	outputVertex.textureCoord = textureCoord;
	

	// Setup local variable pos in case we want to modify it (since position is constant)
	vec4 pos = vec4(position.x, position.y, position.z, 1.0) + posRelParent;
	
	//Work out the transform matrix
	transform = T * R * S;

//Work out the normal for lighting
	mat3 normalMat = transpose(inverse(mat3(transform)));

	outputVertex.normal = normalize(normalMat* normal);

	calculateTBN();

	outputVertex.vert =(transform* pos).xyz;

	//Work out the final pos of the vertex
	gl_Position = camera * transform * pos;
}