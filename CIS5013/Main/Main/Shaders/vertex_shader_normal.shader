#version 330


uniform mat4 T; // Translation matrix
uniform mat4 S; // Scale matrix
uniform mat4 R; // Rotation matrix
uniform mat4 camera; // camera matrix
uniform mat4 projection;

// Input vertex packet
layout (location = 0) in vec4 position;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec4 tangent;
layout (location = 8) in vec2 textureCoord;


// Output vertex packet
out VS_OUT {
    vec3 normal;
	
} vs_out;



void main(void) {

	// Setup local variable pos in case we want to modify it (since position is constant)
	vec4 pos = vec4(position.x, position.y, position.z, 1.0);

	mat4 transform = T * R * S;

	// Apply transformation to pos and store result in gl_Position
	gl_Position = projection * camera * transform * pos;

	mat3 normalMatrix = mat3(transpose(inverse(camera * transform)));
	vs_out.normal = normalize(vec3(projection * vec4(normalMatrix * normal, 0.0)));


}
