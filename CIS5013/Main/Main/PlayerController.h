#pragma once
#include "GameObject.h"
class Game;
class PlayerController : public GameObject
{
public:
	PlayerController(Game * refr);
	~PlayerController();
	void setTarget(GameObject * newTarget) { target = newTarget; }
	void update(double);
	void draw(glm::mat4) {}
	Volume * getVolume() { return nullptr; }
private:
	GameObject* target;
	Game * refr;

	float speed = 0.02f;

};

